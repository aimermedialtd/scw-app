import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import { StatusBar, Platform, View, Text } from "react-native";
import SplashLogo from "./src/components/splash/SplashLogo";
import SplashPartners from "./src/components/splash/SplashPartners";
import { DataController } from "./src/controllers/DataController";
import AppNavigationContainer from "./src/navigation/AppNavigationContainer";
import { Colors } from "./src/utils/Colors";
import { Constants } from "./src/utils/Constants";
import { UtilFunctions } from "./src/utils/UtilFunctions";
import * as ScreenOrientation from "expo-screen-orientation";

export default function App() {
  const [dataIsInitialized, setDataIsInitialized] = useState(false);
  const [appIsReady, setAppIsReady] = useState(false);
  const [onboardingComplete, setOnboardingComplete] = useState(false);

  StatusBar.setBarStyle("dark-content", false);
  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor(Colors.P_20);
  }

  let smallViewport = false;
  if (UtilFunctions.isSmallViewport()) {
    smallViewport = true;
  } else {
    smallViewport = false;
  }

  useEffect(() => {
    prepare();
  }, []);

  useEffect(() => {
    if (dataIsInitialized) {
      pausePartnersScreen();
    }
  }, [dataIsInitialized]);

  const prepare = async () => {
    try {
      // await DataController.clearStorage();

      // lock to portrait on smaller devices (e.g. phones)
      if (Platform.OS !== "web") {
        if (smallViewport) {
          await ScreenOrientation.lockAsync(
            ScreenOrientation.OrientationLock.PORTRAIT_UP
          );
        }
      }

      // check if onboarding screens should be shown
      const isFirstLaunch = await checkIfFirstLaunch();
      DataController.setFirstLaunch(isFirstLaunch);
      if (!isFirstLaunch) setOnboardingComplete(true);

      // init app data from json file
      await DataController.initData();

      // load fonts
      await UtilFunctions.loadFonts();

      // make sure splash logo is visible for a short period
      await new Promise(resolve => setTimeout(resolve, 1500));

      setDataIsInitialized(true);
    } catch (error) {
      console.log("Failed to prepare the app. Error: ", error);
    }
  };

  async function checkIfFirstLaunch() {
    const res = await AsyncStorage.getItem(Constants.STORAGE_KEY_FIRST_LAUNCH);
    if (res === null) {
      // app launching for the first time
      await AsyncStorage.setItem(
        Constants.STORAGE_KEY_FIRST_LAUNCH,
        JSON.stringify(false)
      );
      return true;
    } else {
      // app has launched before
      return false;
    }
  }

  const pausePartnersScreen = async () => {
    await new Promise(resolve => setTimeout(resolve, 1500));
    setAppIsReady(true);
  };

  if (!dataIsInitialized) {
    return <SplashLogo />;
  } else if (!appIsReady) {
    return <SplashPartners />;
  } else {
    return <AppNavigationContainer onboardingComplete={onboardingComplete} />;
  }
}
