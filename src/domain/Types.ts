import { ReactFragment } from "react";
import { TextStyle, ViewStyle } from "react-native";

export type AirtableData = {
  updated: Date;
  reportingPages: ReportingPage[];
  menuItems: MenuItem[];
  completedPages: CompletedPage[];
  addressFields: AddressField[];
  miscellaneousImages: Image[];
  onboardingPages: OnboardingPage[];
};

export type FlatButtonProps = {
  text: string;
  handlePress: () => void;
  filled?: boolean;
};

export type CompletedPage = {
  name: string;
  title: string;
  text: string;
};

export type AddressField = {
  id: string;
  text: string;
  required: boolean;
  answer: string;
};

export type Address = {
  streetNumber: string; // 8
  route: string; // Navarre Street
  city: string; // London
  district: string; // Greater London
  postalCode: string; // E27JH
  region: string; // England
  country: string; // United Kingdom
};

export type AppNavigationProps = {
  initialRoute: string;
};

export type MenuItem = {
  title: string;
  text: string;
};

export type MenuItemProps = {
  title: string;
  text?: string;
  handleMenuItemClick: () => void;
};

export type AppConfiguration = {
  name: string;
  value: number;
};

export type ModalContainerProps = {
  children: ReactFragment;
  visible: boolean;
};

export type OnboardingPage = {
  pageNumber: number;
  image: Image;
  text: string;
};

export type OnboardingPageProps = {
  text: string;
  page: number;
  handleNextButton: () => void;
};

export type ModalProps = {
  title: string;
  text: string;
  visible: boolean;
  buttons: ModalButtons[];
  handleOnRequestClose: () => void;
};

export type ModalButtons = {
  text: string;
  handlePress: () => void;
  fontStyle?: TextStyle;
};

export type ReportingPage = {
  id: string;
  pageNumber: number;
  name: string;
  questions: Question[];
  lastModifiedTime: Date;
};

export type Question = {
  id: string;
  text: string;
  postamble: string;
  info: string;
  type: string;
  images: Image[];
  allowDoNotKnow: boolean;
  compoundScoring?: string;
  required: boolean;
  answered: boolean;
  doNotKnowSelected: boolean;
  choices: Choice[];
  answerText: string; // should only be used for text inputs
  lastModifiedTime: Date;
};

export type ReportProgressProps = {
  title: string;
  pageNumber: number;
};

export type Choice = {
  value: string;
  score: number;
  selected: boolean;
};

export type CompletePageTitleProps = {
  text: string;
};

export type CompletePageTextBodyProps = {
  text: string;
};

export type QuestionContainerProps = {
  // question: Question;
  questionIndex: number;
  // pageNumber: number;
};

export type Image = {
  id: string;
  uri: string;
  width: number;
  height: number;
};

export type Location = {
  latitude: number;
  longitude: number;
};

export type MapComponentProps = {
  question: Question;
  handleManualLocationRequest: () => void;
  handleLocationRequestError: () => void;
  handleLocationConfirmation: (addr: Address) => void;
};

export type AddressComponentProps = {
  question: Question;
  navigation: any;
};

export type PillButtonProps = {
  text: string;
  selected: boolean;
  largeMinWidth: boolean;
  handlePress: () => void;
};

export type DontKnowPillButtonProps = {
  selected: boolean;
  handlePress: () => void;
};

export type BubbleButtonProps = {
  text: string;
  buttonSize?: BubbleButtonSize;
  marginStyle?: ViewStyle;
  borderColour: ViewStyle;
  textColour: TextStyle;
  handlePress: () => void;
  disabled?: boolean;
  showActivityIndicator?: boolean;
};

export enum BubbleButtonSize {
  Small,
  Large,
}

export type WarningBoxProps = {
  text: string;
  customStyling?: ViewStyle;
};

export type CompleteButtonProps = {
  navigation: any;
};

export type MSHFormData = {
  firstName: string;
  lastName: string;
  agreeToSendData: boolean;
  canContact: boolean;
  canShareContact: boolean;
  email: string;
  phone: string;
};
