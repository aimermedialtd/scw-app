import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  Image,
  Platform,
  TouchableHighlight,
  StyleSheet,
  Alert,
  ActivityIndicator,
  useWindowDimensions,
} from "react-native";
import { Address, Location, MapComponentProps } from "../../domain/Types";
import * as ExpoLocation from "expo-location";
import { DataController } from "../../controllers/DataController";
import { Constants } from "../../utils/Constants";
import { Colors } from "../../utils/Colors";
import { UtilFunctions } from "../../utils/UtilFunctions";
import CustomModal from "../shared/CustomModal";

// Mapview must be conditionally imported otherwise an error will occur even if it's not used
let MapView: any;
let Marker: any;
if (Platform.OS === "web") {
  MapView = require("react-native-web-maps").default;
  Marker = require("react-native-web-maps").default.Marker;
} else {
  MapView = require("react-native-maps").default;
  Marker = require("react-native-maps").Marker;
}

export default ({
  question,
  handleManualLocationRequest,
  handleLocationRequestError,
  handleLocationConfirmation,
}: MapComponentProps) => {
  const [location, setLocation] = useState<Location | null>(null);
  const [address, setAddress] = useState<Address | null>(null);
  const [showModal, setShowModal] = useState(false);

  const [manualLocationModalVisible, setManualLocationModalVisible] = useState(false);

  useEffect(() => {
    let isMounted = true;

    ExpoLocation.getCurrentPositionAsync({
      accuracy: ExpoLocation.Accuracy.Low,
    })
      .then(location => {
        // check if this component is still visible because the user may have already cancelled before the map loads
        if (isMounted) {
          setLocation({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          });
        }
      })
      .catch(e => {
        console.log("Failed to get location.");
        handleLocationRequestError();
      });

    return () => {
      isMounted = false;
    };
  }, []);

  useEffect(() => {
    DataController.setLocation(location);
  }, [location]);

  const getAddress = async () => {
    try {
      let chosenAddress: Address = {
        streetNumber: "",
        route: "",
        city: "",
        district: "",
        postalCode: "",
        region: "",
        country: "",
      };

      // use reverse geocoding to translate the latitude and longitude to a proper address
      const res = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${location?.latitude},${location?.longitude}&key=${Constants.MAPS_API_KEY}`
      );
      const json = await res.json();

      // get result with the most amount of address_components (the most data)
      let temp = json.results[0];
      json.results.forEach(item => {
        if (item.address_components.length > temp.address_components.length) {
          temp = item;
        }
      });

      // assign each address component to the appropriate property for the address object
      temp.address_components.forEach(item => {
        if (item.types.includes("street_number")) {
          chosenAddress.streetNumber = item.long_name;
        } else if (item.types.includes("route")) {
          chosenAddress.route = item.long_name;
        } else if (item.types.includes("postal_town")) {
          chosenAddress.city = item.long_name;
        } else if (item.types.includes("administrative_area_level_2")) {
          chosenAddress.district = item.long_name;
        } else if (item.types.includes("administrative_area_level_1")) {
          chosenAddress.region = item.long_name;
        } else if (item.types.includes("country")) {
          chosenAddress.country = item.long_name;
        } else if (item.types.includes("postal_code")) {
          chosenAddress.postalCode = item.long_name;
        }
      });

      // console.log("result: ", chosenAddress);
      setAddress(chosenAddress);
      setShowModal(true);
    } catch (error: any) {
      console.log("Failed to obtain address. Error: ", error);
      Alert.alert("Failed to obtain address", "Try entering it in manually instead.");
    }
  };

  const popupWidth = UtilFunctions.isSmallViewport() ? "90%" : "50%";

  return (
    <View style={styles.container}>
      {location !== null && (
        <MapView
          mapPadding={{
            top: 0,
            right: 0,
            left: 0,
            bottom: 100,
          }}
          provider="google"
          style={styles.map}
          initialRegion={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: 0.002,
            longitudeDelta: 0.002,
          }}
          onRegionChange={region => {
            if (Platform.OS !== "web") {
              setLocation({
                latitude: region.latitude,
                longitude: region.longitude,
              });
            }
          }}
          onRegionChangeComplete={region => {
            if (Platform.OS === "web") {
              setLocation({
                latitude: region.latitude,
                longitude: region.longitude,
              });
            }
          }}
        >
          {/* use a different marker depending on the platform */}
          {Platform.OS === "web" ? (
            <Marker
              coordinate={{
                latitude: location.latitude,
                longitude: location.longitude,
              }}
              icon={require("../../../assets/images/map_marker_small.png")}
            />
          ) : (
            <Marker
              centerOffset={{ x: 0, y: -25 }}
              coordinate={{
                latitude: location.latitude,
                longitude: location.longitude,
              }}
            >
              <Image style={{ width: 45, height: 55 }} source={require("../../../assets/images/map_marker.png")} />
            </Marker>
          )}
        </MapView>
      )}

      {location !== null && (
        <View
          style={[
            styles.popupContainer,
            {
              width: popupWidth,
            },
          ]}
        >
          <Text style={styles.popupQuestion}>{question.text}</Text>
          <Text style={styles.popupQuestionSubtitle}>{question.info}</Text>
          <View style={styles.horizontalSeparator} />
          <View style={styles.popupAnswersContainer}>
            <TouchableHighlight
              underlayColor={Colors.P_20}
              onPress={() => setManualLocationModalVisible(true)}
              style={[styles.popupAnswer, styles.bottomLeftRoundedCorner]}
            >
              <Text style={styles.answerText}>Enter manually</Text>
            </TouchableHighlight>
            <View style={styles.verticalSeparator} />
            <TouchableHighlight
              underlayColor={Colors.P_20}
              onPress={() => getAddress()}
              style={[styles.popupAnswer, styles.bottomRightRoundedCorner]}
            >
              <Text style={styles.answerText}>Confirm Location</Text>
            </TouchableHighlight>
          </View>
        </View>
      )}
      {location === null && <ActivityIndicator size="large" color="#999999" style={{ flex: 1 }} />}
      <CustomModal
        title={`Here is the address we found:`}
        text={`Street: ${address?.streetNumber} ${address?.route}\nCity: ${address?.city}\nDistrict: ${address?.district}\nPostal code: ${address?.postalCode}\nRegion: ${address?.region}\nCountry: ${address?.country}`}
        visible={showModal}
        handleOnRequestClose={() => {}}
        buttons={[
          {
            text: "OK",
            handlePress: () => {
              setShowModal(false);
              handleLocationConfirmation(address!);
            },
            fontStyle: { fontWeight: "bold" },
          },
          {
            text: "Cancel",
            handlePress: () => setShowModal(false),
          },
        ]}
      />
      <CustomModal
        title="Are you sure you want to enter location manually?"
        text="Using our map can provide accurate information on the location of the carwash. If you still wish to enter the location manually, please make sure to provide exact locations."
        visible={manualLocationModalVisible}
        handleOnRequestClose={() => {}}
        buttons={[
          { text: "Use the map", handlePress: () => setManualLocationModalVisible(false) },
          { text: "Enter location manually", handlePress: handleManualLocationRequest },
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  bottomRightRoundedCorner: {
    borderBottomRightRadius: 8,
  },
  bottomLeftRoundedCorner: {
    borderBottomLeftRadius: 8,
  },
  answerText: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 17,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: -0.1,
    textAlign: "center",
    color: Colors.CTA_BLUE,
  },
  popupContainer: {
    height: 122,
    borderRadius: 14,
    backgroundColor: Colors.WHITE,
    alignSelf: "center",
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowRadius: 50,
    shadowOpacity: 1,
    borderStyle: "solid",
    borderWidth: 1,
    zIndex: 1,
    bottom: 20,
    position: "absolute",
    borderColor: "rgba(0, 0, 0, 0.05)",
  },
  popupQuestion: {
    marginTop: 12,
    marginBottom: 4,
    fontSize: 20,
    fontWeight: "500",
    color: Colors.P_80,
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0.1,
    textAlign: "center",
    fontFamily: "AvenirNext-Medium",
  },
  popupQuestionSubtitle: {
    marginBottom: 12,
    textAlign: "center",
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 12,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.GREY_70,
  },
  horizontalSeparator: {
    width: "100%",
    height: 1,
    backgroundColor: "#d1d1d6",
  },
  verticalSeparator: {
    width: 1,
    height: "100%",
    backgroundColor: "#d1d1d6",
  },
  popupAnswersContainer: {
    flexDirection: "row",
    flex: 1,
  },
  popupAnswer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
});
