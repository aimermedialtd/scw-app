import React, { useState } from "react";
import { StyleSheet, TextInput } from "react-native";
import { Question } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import GlobalStyles from "../../utils/GlobalStyles";

export default ({
  multiline = false,
  question,
  handleOnChangeText,
}: {
  multiline?: boolean;
  question: Question;
  handleOnChangeText: (text: string) => void;
}) => {
  return (
    <TextInput
      editable
      multiline={multiline}
      textAlignVertical="top"
      style={
        multiline
          ? [GlobalStyles.textInputMultiline, styles.multi]
          : [GlobalStyles.textInput, styles.single]
      }
      defaultValue={question.answerText}
      onChangeText={text => handleOnChangeText(text)}
    />
  );
};

const styles = StyleSheet.create({
  single: {
    marginTop: 24,
    backgroundColor: Colors.GREY_30,
  },
  multi: {
    marginTop: 16,
    height: 170,
  },
});
