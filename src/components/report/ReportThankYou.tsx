import React, { useLayoutEffect } from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import HTML from "react-native-render-html";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BackButton from "../shared/BackButton";
import BubbleButton from "../shared/BubbleButton";
import CompletePageTitle from "../shared/CompletePageTitle";

export default ({ navigation, route }: any) => {
  const pageContent = DataController.getData().completedPages.find(
    it => it.name === "Thank you (for submitting report)"
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: null,
    });
  }, [navigation]);

  const tagStyles = {
    li: styles.text,
    p: styles.text,
  };

  return (
    <View style={styles.mainContainer}>
      <View
        style={[
          {
            justifyContent: "space-between",
            // backgroundColor: "red",
            flex: 1,
            alignSelf: "center",
          },
          UtilFunctions.isSmallViewport()
            ? styles.smallContainer
            : styles.largeContainer,
        ]}
      >
        <View>
          <CompletePageTitle text={pageContent?.title!} />
          <View style={{ marginTop: 10 }}>
            <HTML
              contentWidth={Dimensions.get("window").width}
              tagsStyles={tagStyles}
              source={{ html: pageContent?.text || "<p></p>" }}
            />
          </View>
        </View>
        <View
          style={{
            marginBottom: 32,
            alignSelf: "center",
          }}
        >
          <BubbleButton
            text="DONE"
            borderColour={styles.buttonBorderColor}
            textColour={styles.buttonTextColor}
            handlePress={() =>
              navigation.reset({
                index: 0,
                routes: [{ name: "Landing" }],
              })
            }
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.P_20,
  },
  buttonBorderColor: {
    borderColor: "#935e11",
  },
  buttonTextColor: {
    color: Colors.P_80,
  },
  smallContainer: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  largeContainer: {
    width: 624,
    paddingLeft: 5,
    paddingRight: 5,
  },
  text: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 22,
    letterSpacing: -0.1,
    color: Colors.P_80,
  },
});
