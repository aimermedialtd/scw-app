import React, { useEffect, useLayoutEffect, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  useWindowDimensions,
  Image,
  Dimensions,
  FlatList,
} from "react-native";
import { CheckBox } from "react-native-elements";
import HTML from "react-native-render-html";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import GlobalStyles from "../../utils/GlobalStyles";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BackButton from "../shared/BackButton";
import BubbleButton from "../shared/BubbleButton";
import CompletePageTitle from "../shared/CompletePageTitle";
import CustomModal from "../shared/CustomModal";

const TextInputView = ({
  label,
  handleTextChange,
  keyboardType,
  customStyling,
}: any) => {
  return (
    <View style={[customStyling]}>
      <Text style={styles.inputLabel}>{label}</Text>
      <TextInput
        keyboardType={keyboardType}
        onChangeText={handleTextChange}
        style={[GlobalStyles.textInput, styles.textInput]}
      />
    </View>
  );
};

const CheckBoxView = ({ text, tagStyles, checked, handlePress }: any) => {
  return (
    <View
      style={{
        marginTop: 8,
        marginBottom: 8,
        flexDirection: "row",
      }}
    >
      <CheckBox
        uncheckedIcon={
          <Image
            style={{ width: 44, height: 44 }}
            source={require("../../../assets/images/list_checkbox_unchecked.png")}
          />
        }
        checkedIcon={
          <Image
            style={{ width: 44, height: 44 }}
            source={require("../../../assets/images/list_checkbox_checked.png")}
          />
        }
        onPress={handlePress}
        containerStyle={{
          marginRight: 0,
          width: 44,
          height: 44,
          justifyContent: "center",
          alignItems: "center",
        }}
        checked={checked}
      />
      <View
        style={{
          flex: 1,
          justifyContent: "center",
        }}
      >
        <HTML
          contentWidth={Dimensions.get("window").width}
          tagsStyles={tagStyles}
          source={{ html: text || "<p></p>" }}
        />
      </View>
    </View>
  );
};

export default ({ navigation, route }: any) => {
  const pageContent = DataController.getData().completedPages.find(
    it => it.name === "Submit report"
  );

  const paragraphArray = pageContent?.text.match(Constants.REG_EX_P_TAG);
  const listArray = pageContent?.text.match(Constants.REG_EX_LI_TAG);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [checkboxesTicked, setCheckboxesTicked] = useState({
    a: false,
    b: false,
    c: false,
  });
  const [showEmailPhoneModal, setShowEmailPhoneModal] = useState(false);
  const [showEmailModal, setShowEmailModal] = useState(false);
  const [showAgreeModal, setShowAgreeModal] = useState(false);

  const [showReportFailedModal, setShowReportFailedModal] = useState(false);
  const [isSubmittingReport, setIsSubmittingReport] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: null,
      headerLeft: () => <BackButton handlePress={() => navigation.pop()} />,
    });
  }, [navigation]);

  const tagStyles = {
    li: styles.text,
    p: styles.text,
  };

  const handleButtonPress = async () => {
    const trimmedFirstName = firstName.trim();
    const trimmedLastName = lastName.trim();
    const trimmedEmail = email.trim();
    const trimmedPhone = phone.trim();

    // make sure email is valid
    if (trimmedEmail !== "") {
      if (!UtilFunctions.isEmailValid(trimmedEmail)) {
        setShowEmailModal(true);
        return;
      }
    }

    // make sure user enters a email or phone number
    if (trimmedEmail === "" && trimmedPhone === "") {
      setShowEmailPhoneModal(true);
      return;
    }

    // make sure user ticks first box
    if (!checkboxesTicked.a) {
      setShowAgreeModal(true);
      return;
    }

    DataController.setMSHFormData({
      firstName: trimmedFirstName,
      lastName: trimmedLastName,
      agreeToSendData: checkboxesTicked.a,
      canContact: checkboxesTicked.b,
      canShareContact: checkboxesTicked.c,
      email: trimmedEmail,
      phone: trimmedPhone,
    });

    // submit report to MSH
    setIsSubmittingReport(true);
    const result = await DataController.submitDataToModernSlaveryHelpline();
    if (result) {
      // successfully submitted report to MSH

      // mark as sent on Aimer server
      await DataController.markReportAsSentToMSH(); // There is a chance that the report to MSH is successfull but this post request fails, thereby not marking the MSH report as sent on Aimer's server (i.e. no checking/error handling).

      navigation.navigate("ReportThankYou");
    } else {
      // failed to submit report
      setShowReportFailedModal(true);
    }
    setIsSubmittingReport(false);
  };

  const data = [
    <CompletePageTitle text={pageContent!.title} />,
    <View style={{ marginTop: 10 }}>
      <HTML
        contentWidth={Dimensions.get("window").width}
        tagsStyles={tagStyles}
        source={{ html: paragraphArray![0] || "<p></p>" }}
      />
    </View>,
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 8,
      }}
    >
      <TextInputView
        label="First name"
        keyboardType="default"
        handleTextChange={setFirstName}
        customStyling={{ marginRight: 5, flex: 1 }}
      />
      <TextInputView
        label="Last name"
        keyboardType="default"
        handleTextChange={setLastName}
        customStyling={{ marginLeft: 5, flex: 1 }}
      />
    </View>,
    <TextInputView
      label="Email address"
      keyboardType="email-address"
      handleTextChange={setEmail}
    />,
    <TextInputView
      label="Telephone"
      keyboardType="numeric"
      handleTextChange={setPhone}
    />,
    <View style={{ marginTop: 12, marginBottom: 4 }}>
      <HTML
        contentWidth={Dimensions.get("window").width}
        tagsStyles={tagStyles}
        source={{ html: paragraphArray![1] || "<p></p>" }}
      />
    </View>,
    <CheckBoxView
      text={listArray![0]}
      tagStyles={tagStyles}
      checked={checkboxesTicked.a}
      handlePress={() =>
        setCheckboxesTicked({
          ...checkboxesTicked,
          a: !checkboxesTicked.a,
        })
      }
    />,
    <CheckBoxView
      text={listArray![1]}
      tagStyles={tagStyles}
      checked={checkboxesTicked.b}
      handlePress={() =>
        setCheckboxesTicked({
          ...checkboxesTicked,
          b: !checkboxesTicked.b,
        })
      }
    />,
    <CheckBoxView
      text={listArray![2]}
      tagStyles={tagStyles}
      checked={checkboxesTicked.c}
      handlePress={() =>
        setCheckboxesTicked({
          ...checkboxesTicked,
          c: !checkboxesTicked.c,
        })
      }
    />,
  ];

  return (
    <View style={{ flex: 1, backgroundColor: Colors.P_20 }}>
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        style={[
          { alignSelf: "center" },
          UtilFunctions.isSmallViewport()
            ? styles.smallContainer
            : styles.largeContainer,
        ]}
        contentContainerStyle={{ flexGrow: 1 }}
        data={data}
        renderItem={({ item, index }) => {
          return item;
        }}
        ListFooterComponent={() => (
          <View
            style={{
              marginBottom: 32,
              marginTop: 16,
              alignSelf: "center",
            }}
          >
            <BubbleButton
              text="NEXT"
              borderColour={styles.buttonBorderColor}
              textColour={styles.buttonTextColor}
              handlePress={handleButtonPress}
              disabled={isSubmittingReport}
              showActivityIndicator={isSubmittingReport}
            />
          </View>
        )}
        ListFooterComponentStyle={{
          flex: 1,
          justifyContent: "flex-end",
        }}
      />
      <CustomModal
        title="Please provide your contact details"
        text="Please provide an email or phone number."
        visible={showEmailPhoneModal}
        buttons={[
          {
            text: "Okay",
            handlePress: () => setShowEmailPhoneModal(false),
          },
        ]}
        handleOnRequestClose={() => setShowEmailPhoneModal(false)}
      />
      <CustomModal
        title="Invalid Email"
        text="It looks like your email address is invalid. Please provide a valid email address."
        visible={showEmailModal}
        buttons={[
          {
            text: "Okay",
            handlePress: () => setShowEmailModal(false),
          },
        ]}
        handleOnRequestClose={() => setShowEmailModal(false)}
      />
      <CustomModal
        title="Please acknowledge"
        text="Please agree to sending your report and contact details to the MSH."
        visible={showAgreeModal}
        buttons={[
          {
            text: "Okay",
            handlePress: () => setShowAgreeModal(false),
          },
        ]}
        handleOnRequestClose={() => setShowAgreeModal(false)}
      />
      <CustomModal
        title="Failed to Submit Report"
        text="Failed to submit your report to the MSH. Please try again."
        visible={showReportFailedModal}
        buttons={[
          {
            text: "Okay",
            handlePress: () => setShowReportFailedModal(false),
          },
        ]}
        handleOnRequestClose={() => setShowReportFailedModal(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  smallContainer: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  largeContainer: {
    width: 624,
    paddingLeft: 5,
    paddingRight: 5,
  },
  buttonBorderColor: {
    borderColor: "#935e11",
  },
  buttonTextColor: {
    color: Colors.P_80,
  },
  textInput: {
    backgroundColor: Colors.WHITE,
  },
  inputLabel: {
    fontFamily: "AvenirNext-Bold",
    fontSize: 13,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.P_50,
    marginBottom: 4,
    marginTop: 8,
  },
  text: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 22,
    letterSpacing: -0.1,
    color: Colors.P_80,
  },
});
