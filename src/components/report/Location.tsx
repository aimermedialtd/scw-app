import React, { useEffect, useState } from "react";
import { View, StyleSheet } from "react-native";
import { DataController } from "../../controllers/DataController";
import { Address } from "../../domain/Types";
import CancelButton from "../shared/CancelButton";
import ReportProgress from "../shared/ReportProgress";
import Map from "./Map";
import * as ExpoLocation from "expo-location";
import CustomModal from "../shared/CustomModal";
import AddressPage from "./AddressPage";

export default ({ navigation, route }: any) => {
  const { page } = route.params;
  const { locationPermissionIsGranted } = route.params;
  const mapQuestion = page.questions[0];
  const addressQuestion = page.questions[1];

  const [showMap, setShowMap] = useState<boolean>(locationPermissionIsGranted);
  const [showLocReqErrModal, setLocReqErrModalVisibility] = useState(false);

  useEffect(() => {
    if (DataController.isDemoMode()) {
      navigation.setOptions({ headerTitle: "Demo mode" });
    }
    navigation.setOptions({
      headerRight: () => <CancelButton navigation={navigation} />,
      headerLeft: null,
    });
  }, [navigation]);

  const handleLocationConfirmation = (address: Address) => {
    DataController.setAddress(address);
    navigation.navigate(DataController.getData().reportingPages[1].name, {
      page: DataController.getData().reportingPages[1],
    });
  };

  return (
    <View style={styles.mainContainer}>
      <ReportProgress title={"LOCATION"} pageNumber={1} />
      {showMap ? (
        <Map
          question={mapQuestion}
          handleManualLocationRequest={() => setShowMap(false)}
          handleLocationRequestError={() => {
            setLocReqErrModalVisibility(true);
          }}
          handleLocationConfirmation={(addr: Address) =>
            handleLocationConfirmation(addr)
          }
        />
      ) : (
        <AddressPage question={addressQuestion} navigation={navigation} />
      )}
      <CustomModal
        title="Failed to get location"
        text="Try entering your address manually instead."
        visible={showLocReqErrModal}
        handleOnRequestClose={() => {
          setLocReqErrModalVisibility(false);
          setShowMap(false);
        }}
        buttons={[
          {
            text: "OK",
            handlePress: () => {
              setLocReqErrModalVisibility(false);
              setShowMap(false);
            },
          },
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
});
