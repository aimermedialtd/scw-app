import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, StyleSheet, Text, TextInput } from "react-native";
import { KeyboardAwareFlatList } from "react-native-keyboard-aware-scroll-view";
import { DataController } from "../../controllers/DataController";
import { Choice, Question } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import GlobalStyles from "../../utils/GlobalStyles";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BackButton from "../shared/BackButton";
import BubbleButton from "../shared/BubbleButton";
import CancelButton from "../shared/CancelButton";
import CustomModal from "../shared/CustomModal";
import ReportProgress from "../shared/ReportProgress";
import QuestionContainer from "./QuestionContainer";

export default ({ navigation, route }: any) => {
  const { page } = route.params;

  const [questions, updateQuestions] = useState<Question[]>(page.questions);
  const [isSubmittingReport, setIsSubmittingReport] = useState(false);
  const [showReportFailedModal, setShowReportFailedModal] = useState(false);

  useLayoutEffect(() => {
    if (DataController.isDemoMode()) {
      navigation.setOptions({ headerTitle: "Demo mode" });
    }
    navigation.setOptions({
      headerRight: () => <CancelButton navigation={navigation} />,
      headerLeft: () => <BackButton handlePress={() => navigation.pop()} />,
    });
  }, [navigation]);

  const handleOnChangeText = (text: string, index: number) => {
    const copy = questions.slice();
    copy[index].answerText = text;
    copy[index].answered = text.trim() !== "";

    updateQuestions(copy);
  };

  const handlePillSelection = (choiceSelected: Choice, index: number) => {
    const copy = questions.slice();
    copy[index].doNotKnowSelected = false;
    const choices = copy[index].choices;
    choices.forEach(c => {
      c.selected = c.value === choiceSelected.value;
    });

    if (UtilFunctions.checkIfQuestionMatchesSet(copy[index])) {
      const yesSelected = choiceSelected.value === "YES";
      const textInputHasContent = copy[index].answerText !== "";
      if (!yesSelected) copy[index].answerText = "";
      copy[index].answered = (yesSelected && textInputHasContent) || !yesSelected;
    } else {
      copy[index].answered = choices.some(c => c.selected);
    }

    updateQuestions(copy);
  };

  const handleCheckBoxListSelection = (choiceSelected: Choice, index: number, isLastChoice: boolean) => {
    const copy = questions.slice();
    const choices = copy[index].choices;
    choices.find(c => c.value === choiceSelected.value)!.selected = !choices.find(c => c.value === choiceSelected.value)!.selected;

    // clear all other selections in the list if last choice was selected
    if (isLastChoice) {
      choices.forEach((choice, index, array) => {
        if (index !== array.length - 1) {
          choice.selected = false;
        }
      });
    } else {
      choices[choices.length - 1].selected = false;
    }
    copy[index].answered = isLastChoice ? copy[index].answerText !== "" : choices.some(choice => choice.selected === true);

    if (!isLastChoice) copy[index].answerText = "";

    updateQuestions(copy);
  };

  const shouldButtonBeDisabled = () => {
    const requiredQuestions = questions.filter(question => question.required);
    if (requiredQuestions.length == 0) {
      return false;
    } else {
      if (requiredQuestions.every(question => question.answered)) {
        return false;
      } else {
        return true;
      }
    }
  };

  const handleNextButton = async () => {
    // update the data controller with the questions in this page
    questions.forEach((q, index) => {
      DataController.updateQuestionForPage(page.pageNumber, index, q);
    });

    const nextIndex = page.pageNumber;
    // move to next page if it exists, othersise move to the "complete" page for appropriate mode and score
    if (DataController.getData().reportingPages[nextIndex]) {
      navigation.navigate(DataController.getData().reportingPages[nextIndex].name, {
        page: DataController.getData().reportingPages[nextIndex],
      });
    } else {
      let route: string = "";
      if (DataController.isDemoMode()) {
        navigation.reset({
          index: 0,
          routes: [{ name: "CompleteDemo" }],
        });
      } else {
        setIsSubmittingReport(true);
        const result = await DataController.submitReportToAimerServer();
        setIsSubmittingReport(false);
        if (result) {
          navigation.reset({
            index: 0,
            routes: [{ name: "Complete" }],
          });
        } else {
          setShowReportFailedModal(true);
        }
      }
    }
  };

  const getButtonText = () => {
    if (DataController.getData().reportingPages.length === page.pageNumber) {
      return "SUBMIT";
    } else {
      return "NEXT";
    }
  };

  const handleDontKnowSelection = (index: number) => {
    const copy = questions.slice();
    copy[index].doNotKnowSelected = true;
    copy[index].answered = true;
    const choices = copy[index].choices;
    choices.forEach(c => {
      c.selected = false;
    });

    updateQuestions(copy);
  };

  return (
    <View style={styles.mainContainer}>
      <ReportProgress title={page.name} pageNumber={page.pageNumber} />
      <KeyboardAwareFlatList
        data={questions}
        showsVerticalScrollIndicator={true}
        alwaysBounceVertical={false}
        style={[UtilFunctions.isSmallViewport() ? styles.smallDisplayList : styles.largeDisplayList]}
        contentContainerStyle={styles.contentContainerStyle}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        ListFooterComponent={
          <View style={styles.footerContainer}>
            <BubbleButton
              disabled={shouldButtonBeDisabled()}
              borderColour={styles.buttonBorder}
              textColour={styles.buttonText}
              showActivityIndicator={isSubmittingReport}
              handlePress={handleNextButton}
              text={getButtonText()}
            />
          </View>
        }
        ListFooterComponentStyle={styles.footerComponentStyle}
        renderItem={({ item, index }) => (
          <QuestionContainer
            question={item}
            questionIndex={index}
            handleOnChangeText={text => handleOnChangeText(text, index)}
            handlePillSelection={choiceSelected => handlePillSelection(choiceSelected, index)}
            handleCheckBoxListSelection={(choiceSelected, isLastChoice) => handleCheckBoxListSelection(choiceSelected, index, isLastChoice)}
            handleDontKnowSelection={() => handleDontKnowSelection(index)}
            handleExtraInput={text => handleOnChangeText(text, index)}
          />
        )}
      />
      <CustomModal
        title="Failed to Submit Report"
        text="Failed to submit your report. Please try again."
        visible={showReportFailedModal}
        buttons={[
          {
            text: "Okay",
            handlePress: () => setShowReportFailedModal(false),
          },
        ]}
        handleOnRequestClose={() => setShowReportFailedModal(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: Colors.WHITE,
  },
  multi: {
    marginTop: 8,
    height: 100,
  },
  smallDisplayList: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  largeDisplayList: {
    width: 624,
    paddingLeft: 5,
    paddingRight: 5,
  },
  divider: {
    backgroundColor: Colors.DIVIDER,
    height: 1,
    marginTop: 20,
  },
  buttonBorder: {
    borderColor: Colors.SECONDARY_ORANGE,
  },
  extraText: {
    marginTop: 12,
    fontSize: 15,
    lineHeight: 20,
    letterSpacing: -0.1,
    color: Colors.GREY_70,
    fontWeight: "500",
    textAlign: "center",
    fontStyle: "normal",
    fontFamily: "AvenirNext-Medium",
  },
  buttonText: {
    color: Colors.SECONDARY_ORANGE,
  },
  footerContainer: {
    marginBottom: 32,
    marginTop: 32,
    alignItems: "center",
  },
  footerComponentStyle: {
    flex: 1,
    justifyContent: "flex-end",
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
});
