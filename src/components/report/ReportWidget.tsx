import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { View, StyleSheet, Text, Platform } from "react-native";
import WebView from "react-native-webview";
import { UtilFunctions } from "../../utils/UtilFunctions";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";

// *************************************************************************
// This component is unused at the moment but it may be used at a later date
// *************************************************************************

export default ({ navigation, route }: any) => {
  const webViewRef = useRef<WebView>(null);

  const [description, setDescription] = useState(DataController.buildDescriptionString());
  const [location, setLocation] = useState(DataController.buildLocationString());

  const js = `
  try {
    function loadPageEnd() {
        document.getElementsByName('enquirytype')[0].value = "I think I have witnessed examples of modern slavery or human trafficking";
        document.getElementsByName('description')[0].textContent = '${description}';
        document.getElementsByName('location')[0].value = '${location}';

        window.ReactNativeWebView.postMessage(JSON.stringify({error: false}));
        true; // note: this is required, or you'll sometimes get silent failures
    }
    window.onload = loadPageEnd();
} catch(e) {
    window.ReactNativeWebView.postMessage(JSON.stringify({error: true}));
}
    `;

  useEffect(() => {}, []);

  useLayoutEffect(() => {
    if (DataController.isDemoMode()) {
      navigation.setOptions({ headerTitle: "Demo mode" });
    }
    navigation.setOptions({
      headerRight: () => (
        <Text style={styles.skipButton} onPress={() => navigation.navigate("ReportThankYou")}>
          Skip
        </Text>
      ),
    });
  }, [navigation]);

  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      <View style={[styles.container, UtilFunctions.isSmallViewport() ? styles.smallContainer : styles.largeContainer]}>
        {Platform.OS === "web" ? (
          //   iframe for web
          <iframe
            onLoad={() => {}}
            height={"100%"}
            style={{ border: "none" }}
            src="https://www.modernslaveryhelpline.org/widget/iframe.php?id=9mc28kvrjf"
          />
        ) : (
          // webview for android/ios
          <WebView
            ref={webViewRef}
            onLoadEnd={() => setTimeout(() => webViewRef.current?.injectJavaScript(js), 300)}
            bounces={false}
            javaScriptEnabled
            domStorageEnabled
            style={[]}
            originWhitelist={["*"]}
            source={{
              uri: "https://www.modernslaveryhelpline.org/widget/iframe.php?id=9mc28kvrjf",
            }}
            onMessage={event => console.log("failed to inject js: ", event.nativeEvent.data)}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: "center",
    backgroundColor: "white",
  },
  smallContainer: {
    width: "100%",
  },
  largeContainer: {
    width: 624,
  },
  skipButton: {
    marginRight: 16,
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    color: Colors.P_80,
  },
});
