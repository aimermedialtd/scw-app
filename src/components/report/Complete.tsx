import React, { useEffect, useState } from "react";
import { Text, StyleSheet, Image, ScrollView, View, StatusBar, Linking, ImageBackground } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import CompletePageTextBody from "../shared/CompletePageTextBody";
import CompletePageTitle from "../shared/CompletePageTitle";
import CustomModal from "../shared/CustomModal";
import FlatBubbleButton from "../shared/FlatBubbleButton";
import WarningBox from "../shared/WarningBox";

const CallButton = () => {
  const handleCallNow = () => {
    try {
      Linking.openURL(`tel:${Constants.MODERN_SLAVERY_PHONE_NUMBER}`);
    } catch (error) {
      console.log("Error with calling Modern Slavery Helpline");
    }
  };

  return (
    <TouchableOpacity onPress={handleCallNow} style={[styles.callButtonContainer]}>
      <ImageBackground
        style={styles.callButtonImageBackground}
        source={require("../../../assets/images/button_call_now.png")}
      >
        <Image style={styles.callButtonImage} source={require("../../../assets/images/phone.png")} />
        <Text style={[styles.horizontalButtonText, styles.callButtonText]}>CALL HELPLINE</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default ({ navigation, route }: any) => {
  const [submitReportModalVisible, setSubmitReportModalVisible] = useState(false);

  const modernSlaveryLikely = DataController.isModernSlaveryLikely();
  const modernSlaveryLikelyPage = DataController.getData().completedPages.find(
    it => it.name === "Modern Slavery Likely"
  );
  const modernSlaveryUnlikelyPage = DataController.getData().completedPages.find(
    it => it.name === "Modern Slavery Unlikely"
  );

  let completedPageToShow;
  if (modernSlaveryLikely) {
    completedPageToShow = modernSlaveryLikelyPage;
  } else {
    completedPageToShow = modernSlaveryUnlikelyPage;
  }

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image style={{ marginLeft: 5, width: 44, height: 44 }} source={require("../../../assets/images/menu.png")} />
        </TouchableOpacity>
      ),
    });
  }, []);

  return (
    <ScrollView
      style={styles.mainContainer}
      contentContainerStyle={{
        alignItems: "center",
        flexGrow: 1,
        justifyContent: "space-between",
      }}
    >
      <StatusBar hidden={false} backgroundColor={Colors.P_20} barStyle="dark-content" />
      <CompletePageTitle text={completedPageToShow!.title} />
      <CompletePageTextBody text={completedPageToShow!.text} />
      <View style={styles.bottomPinnedContainer}>
        <View style={{ marginTop: 16, marginBottom: 34 }}>
          {modernSlaveryLikely && (
            <>
              <FlatBubbleButton
                text="SUBMIT REPORT"
                filled
                handlePress={() => navigation.navigate("ReportSubmissionDetails")}
              />
              <CallButton />
            </>
          )}
          <View style={{ marginTop: 16 }}>
            <FlatBubbleButton
              text={"BACK TO START"}
              handlePress={() => {
                if (modernSlaveryLikely) {
                  setSubmitReportModalVisible(true);
                } else {
                  navigation.replace("Landing");
                }
              }}
            />
          </View>
        </View>
        <WarningBox customStyling={{ marginBottom: 24 }} text={Constants.STR_WARNING_BOX_TEXT_COMPLETE} />
      </View>
      <CustomModal
        title="Please consider submitting your report"
        text="Your report could provide valuable information to the Modern Slavery Helpline and help in the fight against modern slavery."
        visible={submitReportModalVisible}
        handleOnRequestClose={() => {}}
        buttons={[
          {
            text: "Yes, submit my report",
            handlePress: () => {
              setSubmitReportModalVisible(false);
              navigation.navigate("ReportSubmissionDetails");
            },
          },
          {
            text: "No, I don't want to submit",
            handlePress: () => {
              setSubmitReportModalVisible(false);
              navigation.replace("Landing");
            },
          },
        ]}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  bodyText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    textAlign: "center",
    marginTop: 8,
    color: Colors.P_80,
  },
  bottomPinnedContainer: {
    marginTop: "auto",
    width: "100%",
    alignItems: "center",
  },
  callButtonImage: {
    width: 35,
    height: 35,
    marginRight: 1,
    // marginLeft: 53,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 0,
    shadowOpacity: 1,
  },
  callButtonImageBackground: {
    width: 275,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  callButtonContainer: {
    flexDirection: "row",
    marginTop: 16,
  },
  horizontalButtonText: {
    fontFamily: "AvenirNext-Bold",
    fontSize: 22,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30,
    letterSpacing: 0.2,
  },
  horizontalButtonContainer: {
    width: 275,
    height: 50,
    borderRadius: 25,
    borderStyle: "solid",
    alignItems: "center",
  },
  callButtonText: {
    color: Colors.WHITE,
    textShadowColor: "rgba(0, 0, 0, .1)",
    textShadowOffset: {
      width: 0,
      height: 2,
    },
    textShadowRadius: 0,
  },
  flatButtonText: {
    color: Colors.P_80,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.P_20,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
