import React, { useEffect } from "react";
import { Text, View, StyleSheet, Image, TextInput } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Choice, Question } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import GlobalStyles from "../../utils/GlobalStyles";
import { UtilFunctions } from "../../utils/UtilFunctions";
import DontKnowPillButton from "../shared/DontKnowPillButton";
import CheckBoxListQuestionType from "./CheckBoxListQuestionType";
import MultipleChoiceQuestionType from "./MultipleChoiceQuestionType";
import TextInputQuestionType from "./TextInputQuestionType";

export default ({
  question,
  questionIndex,
  handleOnChangeText,
  handlePillSelection,
  handleCheckBoxListSelection,
  handleDontKnowSelection,
  handleExtraInput,
}: {
  question: Question;
  questionIndex: number;
  handleOnChangeText: (text: string) => void;
  handlePillSelection: (choiceSelected: Choice) => void;
  handleCheckBoxListSelection: (choiceSelected: Choice, isLastChoice: boolean) => void;
  handleDontKnowSelection: () => void;
  handleExtraInput: (text: string) => void;
}) => {
  const matchesId = UtilFunctions.checkIfQuestionMatchesSet(question);
  const selectedChoices = question.choices.filter(c => c.selected);
  let yesSelected = false;
  if (selectedChoices.length > 0) {
    yesSelected = selectedChoices[0].value === "YES";
  }
  let extraInputRequired = false;
  if (question.type === "CheckboxList") {
    extraInputRequired = matchesId && selectedChoices.length > 0 && selectedChoices[0].value === "NO PPE";
  } else {
    extraInputRequired = matchesId && yesSelected;
  }
  let extraInputText = "";
  if (question.id === Constants.LIVING_ONSITE_QUESTION_ID || question.id === Constants.CLOTHING_QUESTION_ID) {
    extraInputText = "Please give details of what you saw (required).";
  } else if (question.id === Constants.OVER_CONTROLLER_OR_INTIMIDATING_ID) {
    extraInputText = "Please describe what you have seen in more detail (required).";
  } else {
    extraInputText =
      "Please give us more detail on the type of work they are doing and anything you may have noticed about their treatment and/or demeanour (required).";
  }

  function displayAnswerFieldsForQuestionType() {
    switch (question.type) {
      case "SingleLineText": {
        return <TextInputQuestionType handleOnChangeText={handleOnChangeText} question={question} />;
      }
      case "MultipleLineText": {
        return <TextInputQuestionType question={question} handleOnChangeText={handleOnChangeText} multiline={true} />;
      }
      case "CheckboxList": {
        return (
          <CheckBoxListQuestionType question={question} handleCheckBoxListSelection={handleCheckBoxListSelection} />
        );
      }
      case "YES/NO":
      case "MultipleChoice": {
        return <MultipleChoiceQuestionType question={question} handlePillSelection={handlePillSelection} />;
      }
    }
  }

  function getImageSource(questionId: string) {
    switch (questionId) {
      case "recJMCGb26vtp6vPP":
        return require("../../../assets/images/living.png");
      case "recMc0jJGlY2OZi4F":
        return require("../../../assets/images/carwash-type.png");
      default:
        throw "question id unknown";
    }
  }

  return (
    <View style={styles.mainContainer}>
      {/* question text */}
      <Text style={[styles.questionText, { marginTop: questionIndex == 0 ? 32 : 20 }]}>{question.text}</Text>
      {/* info */}
      {question.info && <Text style={styles.questionInfo}>{question.info}</Text>}
      {/* answer field */}
      {displayAnswerFieldsForQuestionType()}
      {/* don't know button */}
      {question.allowDoNotKnow && (
        <DontKnowPillButton selected={question.doNotKnowSelected} handlePress={handleDontKnowSelection} />
      )}
      {/* display an extra text input field if questions 9/12/13 are answered yes */}
      {extraInputRequired && (
        <>
          <Text style={styles.questionInfo}>{extraInputText}</Text>
          <TextInput
            editable
            textAlignVertical="top"
            multiline
            onChangeText={text => handleExtraInput(text)}
            defaultValue={question.answerText}
            style={[GlobalStyles.textInput, { backgroundColor: Colors.GREY_30, marginTop: 8, height: 100 }]}
          />
        </>
      )}
      {/* postamble */}
      {question.postamble && <Text style={styles.questionPostamble}>{question.postamble}</Text>}
      {/* images */}
      {question.images &&
        question.images.map(image => {
          return (
            <View key={image.uri}>
              <Image
                resizeMode="contain"
                style={{
                  aspectRatio: image.width / image.height,
                  width: "100%",
                  height: 150,
                }}
                source={getImageSource(question.id)}
              />
            </View>
          );
        })}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: "center",
  },
  questionText: {
    fontFamily: "AvenirNext-Medium",
    fontSize: 20,
    lineHeight: 28,
    letterSpacing: 0.1,
    color: Colors.P_80,
    fontWeight: "500",
    textAlign: "center",
    fontStyle: "normal",
  },
  questionPostamble: {
    marginTop: 24,
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 15,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: -0.1,
    textAlign: "center",
    color: Colors.P_80,
  },
  questionInfo: {
    marginTop: 4,
    fontSize: 15,
    lineHeight: 20,
    letterSpacing: -0.1,
    color: Colors.GREY_70,
    fontWeight: "500",
    textAlign: "center",
    fontStyle: "normal",
    fontFamily: "AvenirNext-Medium",
  },
});
