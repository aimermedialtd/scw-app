import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { Choice, Question } from "../../domain/Types";
import PillButton from "../shared/PillButton";

export default ({
  question,
  handlePillSelection,
}: {
  question: Question;
  handlePillSelection: (choiceSelected: Choice) => void;
}) => {
  return (
    <View style={styles.mainContainer}>
      {question.choices.map((choice, _, array) => {
        return (
          <PillButton
            key={choice.value}
            selected={choice.selected}
            text={choice.value}
            largeMinWidth={array.length === 2}
            handlePress={() => handlePillSelection(choice)}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    marginTop: 8,
    marginBottom: 4,
  },
});
