import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  useWindowDimensions,
} from "react-native";
import { KeyboardAwareFlatList } from "react-native-keyboard-aware-scroll-view";
import { DataController } from "../../controllers/DataController";
import { AddressComponentProps, AddressField } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import GlobalStyles from "../../utils/GlobalStyles";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BubbleButton from "../shared/BubbleButton";

export default ({ question, navigation }: AddressComponentProps) => {
  // const viewportWidth = useWindowDimensions().width;
  const viewportWidth = 100;

  const [addressFields, updateAddressFields] = useState<AddressField[]>(
    DataController.getData().addressFields
  );

  const handleNextButton = () => {
    const page = DataController.getData().reportingPages[1];
    navigation.navigate(page.name, { page: page });
  };

  const shouldButtonBeDisabled = () => {
    const requiredFields = addressFields.filter(field => field.required);
    if (requiredFields.length == 0 && postcodeIsValid()) {
      return false;
    } else {
      if (
        requiredFields.every(field => field.answer.trim() !== "") &&
        postcodeIsValid()
      ) {
        return false;
      } else {
        return true;
      }
    }
  };

  const handleTextInputChange = (
    text: string,
    index: number,
    item: AddressField
  ) => {
    const addressFieldsCopy = [...addressFields];
    addressFieldsCopy[index].answer = text;

    updateAddressFields(addressFieldsCopy);
  };

  const postcodeIsValid = () => {
    const postcodeField = addressFields.find(
      item => item.id === Constants.POSTCODE_ID
    );

    if (postcodeField) {
      const postcodeRegEx =
        /^\b(GIR ?0AA|SAN ?TA1|(?:[A-PR-UWYZ](?:\d{0,2}|[A-HK-Y]\d|[A-HK-Y]\d\d|\d[A-HJKSTUW]|[A-HK-Y]\d[ABEHMNPRV-Y])) ?\d[ABD-HJLNP-UW-Z]{2})\b$/gim;
      if (
        postcodeField.answer === "" ||
        postcodeRegEx.test(postcodeField.answer)
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  return (
    <View style={styles.mainContainer}>
      <KeyboardAwareFlatList
        keyExtractor={item => item.text}
        style={[
          styles.addressFieldsContainer,
          UtilFunctions.isSmallViewport()
            ? styles.smallDisplay
            : styles.largeDisplay,
        ]}
        data={addressFields}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => {
          const isPostcodeField = item.id === Constants.POSTCODE_ID;
          return (
            <View style={styles.fieldContainer}>
              <Text style={styles.addressFieldText}>
                {item.required
                  ? `${item.text} (required)`
                  : `${item.text} (optional)`}
              </Text>
              <TextInput
                editable
                autoCapitalize={isPostcodeField ? "characters" : "words"}
                onChangeText={text => handleTextInputChange(text, index, item)}
                style={[
                  GlobalStyles.textInput,
                  { marginTop: 4, backgroundColor: Colors.GREY_30 },
                ]}
              />
              {isPostcodeField && !postcodeIsValid() && (
                <Text style={styles.invalidPostcodeText}>
                  Invalid postcode entered!
                </Text>
              )}
            </View>
          );
        }}
        contentContainerStyle={styles.contentContainerStyle}
        ListHeaderComponent={<Text style={styles.title}>{question.text}</Text>}
        ListFooterComponentStyle={styles.footerComponentStyle}
        ListFooterComponent={
          <View>
            <BubbleButton
              disabled={shouldButtonBeDisabled()}
              borderColour={styles.buttonBorder}
              textColour={styles.buttonText}
              text="NEXT"
              handlePress={handleNextButton}
            />
          </View>
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  addressFieldText: {
    marginTop: 16,
    alignSelf: "flex-start",
  },
  buttonBorder: {
    borderColor: Colors.SECONDARY_ORANGE,
  },
  buttonText: {
    color: Colors.SECONDARY_ORANGE,
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  fieldContainer: {
    width: "100%",
  },
  invalidPostcodeText: {
    color: "red",
    marginTop: 8,
  },
  smallDisplay: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  largeDisplay: {
    width: 624,
    paddingLeft: 5,
    paddingRight: 5,
  },
  footerComponentStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    // backgroundColor: "yellow",
  },
  formContainer: {
    flex: 1,
  },
  addressFieldsContainer: {
    flex: 1,
    paddingBottom: 32,
    alignSelf: "center",
  },
  title: {
    fontFamily: "AvenirNext-Medium",
    fontSize: 20,
    lineHeight: 28,
    letterSpacing: 0.1,
    color: Colors.P_80,
    fontWeight: "500",
    textAlign: "center",
    fontStyle: "normal",
    marginTop: 32,
  },
});
