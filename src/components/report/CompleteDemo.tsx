import React, { useEffect } from "react";
import { Image, StyleSheet, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";
import CompletePageTextBody from "../shared/CompletePageTextBody";
import CompletePageTitle from "../shared/CompletePageTitle";
import FlatBubbleButton from "../shared/FlatBubbleButton";

export default ({ navigation }: any) => {
  const demoPage = DataController.getData().completedPages.find(it => it.name === "Demo");

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image style={{ marginLeft: 5, width: 44, height: 44 }} source={require("../../../assets/images/menu.png")} />
        </TouchableOpacity>
      ),
    });
  }, []);

  return (
    <View style={styles.mainContainer}>
      <View>
        <CompletePageTitle text={demoPage!.title} />
        <CompletePageTextBody text={demoPage!.text} />
      </View>
      <View style={{ width: "100%", alignItems: "center", marginBottom: 44 }}>
        <FlatBubbleButton text={"BACK TO START"} handlePress={() => navigation.replace("Landing")} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: Colors.P_20,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
