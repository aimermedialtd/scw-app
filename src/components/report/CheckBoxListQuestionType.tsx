import React, { useEffect } from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { CheckBox } from "react-native-elements";
import { Choice, Question } from "../../domain/Types";
import { Colors } from "../../utils/Colors";

export default ({
  question,
  handleCheckBoxListSelection,
}: {
  question: Question;
  handleCheckBoxListSelection: (choiceSelected: Choice, isLastChoice: boolean) => void;
}) => {
  return (
    <View style={styles.mainContainer}>
      {question.choices.map((choice, index, array) => {
        return (
          <View key={choice.value}>
            <View style={styles.row}>
              <Text
                style={[
                  styles.text,
                  {
                    color: choice.selected ? "#e39c00" : Colors.SECONDARY_ORANGE,
                  },
                ]}
              >
                {choice.value}
              </Text>
              <CheckBox
                uncheckedIcon={<Image style={styles.box} source={require("../../../assets/images/list_checkbox_unchecked.png")} />}
                checkedIcon={<Image style={styles.box} source={require("../../../assets/images/list_checkbox_checked.png")} />}
                onPress={() => handleCheckBoxListSelection(choice, index === array.length - 1)}
                containerStyle={styles.checkBoxContainer}
                checked={choice.selected}
              />
            </View>
            <View style={styles.divider} />
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    marginTop: 16,
    marginBottom: 8,
  },
  checkBoxContainer: {
    marginRight: 0,
    height: "100%",
    width: 44,
    justifyContent: "center",
    alignItems: "center",
  },
  row: {
    height: 44,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  divider: {
    backgroundColor: "rgba(177, 127, 30, 0.2)",
    height: 1,
    width: "100%",
  },
  box: {
    width: 44,
    height: 44,
  },
  text: {
    fontFamily: "AvenirNext-Bold",
    fontSize: 17,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: -0.1,
  },
});
