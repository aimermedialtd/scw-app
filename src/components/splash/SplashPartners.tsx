import React from "react";
import {
  Image,
  Text,
  View,
  StyleSheet,
  Platform,
  StatusBar,
} from "react-native";
import { Colors } from "../../utils/Colors";

export default () => {
  return (
    <View style={styles.containerWrapper}>
      <View style={styles.container}>
        <Text style={styles.title}>In partnership with</Text>
        <Image
          resizeMode="contain"
          style={styles.imageGlaa}
          source={require("../../../assets/images/splash-glaa.png")}
        />
        <Image
          resizeMode="contain"
          style={styles.imageNca}
          source={require("../../../assets/images/splash-nca.png")}
        />
        <Image
          resizeMode="contain"
          style={styles.imageNpcc}
          source={require("../../../assets/images/splash-npcc.png")}
        />
        <Image
          resizeMode="contain"
          style={styles.imageIasc}
          source={require("../../../assets/images/splash-iasc.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: Colors.P_20,
  },
  containerWrapper: {
    flex: 1,
    alignItems: "center",
    backgroundColor: Colors.P_20,
  },
  title: {
    // marginTop: 60,
    alignSelf: "center",
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    textAlign: "center",
    color: Colors.P_80,
  },
  imageGlaa: {
    height: 70,
    width: 200,
    marginTop: 40,
  },
  imageIasc: {
    height: 90,
    width: 70,
    marginTop: 40,
  },
  imageNca: {
    height: 55,
    width: 150,
    marginTop: 40,
  },
  imageNpcc: {
    height: 50,
    width: 140,
    marginTop: 40,
  },
});
