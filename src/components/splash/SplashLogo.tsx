import React from "react";
import { Image, View, StyleSheet } from "react-native";
import { Colors } from "../../utils/Colors";

export default () => {
  return (
    <View style={styles.mainContainer}>
      <Image
        resizeMode="contain"
        style={styles.logo}
        source={require("../../../assets/images/splash-logo.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 400,
    height: 400,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.P_20,
    justifyContent: "center",
    alignItems: "center",
  },
});
