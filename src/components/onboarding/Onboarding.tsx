import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { BackHandler, Text, View, SafeAreaView, StyleSheet, StatusBar, Platform } from "react-native";
import Swiper from "react-native-web-swiper";
import { DataController } from "../../controllers/DataController";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import OnboardingPage from "./OnboardingPage";
import { TouchableOpacity } from "react-native-gesture-handler";

export default ({ navigation, route }: any) => {
  const pages = DataController.getData().onboardingPages;

  const [skipButtonVisibility, setSkipButtonVisibility] = useState(false);

  const swiper = useRef<Swiper>(null);
  const index = useRef(0);

  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor(Colors.WHITE, true);
    // StatusBar.setTranslucent(true);
  }

  useEffect(() => {
    handleIndexChange(index.current);
    const backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      if (index.current === 0) {
        return false;
      } else {
        swiper.current?.goToPrev();
        return true;
      }
    });

    return () => {
      backHandler.remove();
    };
  }, []);

  const handleNextButton = () => {
    if (index.current !== pages.length - 1) {
      swiper.current?.goToNext();
    } else {
      navigation.replace("Landing");
    }
  };

  const handleIndexChange = (newIndex: number) => {
    index.current = newIndex;
    if (newIndex !== pages.length - 1) {
      setSkipButtonVisibility(true);
    } else {
      setSkipButtonVisibility(false);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={{
          backgroundColor: Colors.WHITE,
          paddingTop: StatusBar.currentHeight,
        }}
      >
        <View style={[styles.header, {}]}>
          {skipButtonVisibility && (
            <TouchableOpacity style={styles.skipButton} onPress={() => navigation.replace("Landing")}>
              <Text style={styles.skipText}>Skip</Text>
            </TouchableOpacity>
          )}
        </View>
      </SafeAreaView>
      <Swiper
        containerStyle={{ backgroundColor: Colors.WHITE }}
        springConfig={{ tension: 1 }}
        ref={swiper}
        controlsProps={{
          prevTitle: "",
          nextTitle: "",
          prevPos: false,
          nextPos: false,
          dotsWrapperStyle: { marginBottom: 40 },
          dotProps: {
            badgeStyle: { backgroundColor: "#DF9E0D" },
          },
          dotActiveStyle: { backgroundColor: "#5B3F00" },
        }}
        loop={false}
        onIndexChanged={index => handleIndexChange(index)}
      >
        {pages.map(item => {
          return (
            <OnboardingPage
              key={item.pageNumber}
              text={item.text}
              // image={item.image}
              page={item.pageNumber}
              handleNextButton={handleNextButton}
            />
          );
        })}
      </Swiper>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 44,
    backgroundColor: Colors.WHITE,
    alignItems: "flex-end",
  },
  skipButton: {
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    width: 65,
  },
  skipText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    color: Colors.P_80,
  },
});
