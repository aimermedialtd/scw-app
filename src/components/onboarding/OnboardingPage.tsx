import React from "react";
import { View, Image, ScrollView, Text, StyleSheet } from "react-native";
import { BubbleButtonSize, OnboardingPageProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BubbleButton from "../shared/BubbleButton";

export default ({ text, page, handleNextButton }: OnboardingPageProps) => {
  // const imageSource = `../../../assets/images/ob-${page}.png`;

  function getImageSource() {
    switch (page) {
      case 1:
        return require("../../../assets/images/ob-1.png");
      case 2:
        return require("../../../assets/images/ob-2.png");
      case 3:
        return require("../../../assets/images/ob-3.png");
      case 4:
        return require("../../../assets/images/ob-4.png");
      case 5:
        return require("../../../assets/images/ob-5.png");
      default:
        throw "onboarding page unknown";
    }
  }

  return (
    <View style={[styles.mainContainer]}>
      <View style={styles.containerTopHalf}>
        <Image
          resizeMode="contain"
          style={{
            flex: 1,
            // aspectRatio: image.width / image.height, // or 3 / 2
            // width: image.width,
            // height: image.height,
            aspectRatio: 1125 / 708, // or 3 / 2
            width: 1125,
            height: 708,
          }}
          // source={{ uri: image.uri }}
          source={getImageSource()}
        />
      </View>
      <View style={styles.containerBottomHalf}>
        <Image style={styles.imgLogo} source={require("../../../assets/images/logo.png")} />
        <ScrollView
          alwaysBounceVertical={false}
          style={[
            styles.textScrollView,
            {
              width: UtilFunctions.isSmallViewport() ? "100%" : 624,
            },
          ]}
        >
          <Text style={styles.text}>{text}</Text>
        </ScrollView>
        <BubbleButton
          handlePress={handleNextButton}
          buttonSize={BubbleButtonSize.Small}
          marginStyle={styles.buttonMargin}
          textColour={styles.buttonText}
          borderColour={styles.buttonBorder}
          text="NEXT"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonBorder: {
    borderColor: Colors.P_70,
  },
  buttonMargin: {
    marginBottom: 60,
  },
  buttonText: {
    color: Colors.P_80,
  },
  containerTopHalf: {
    height: "40%",
    alignItems: "center",
  },
  containerBottomHalf: {
    height: "60%",
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: Colors.P_20,
    alignItems: "center",
  },
  imgLogo: {
    width: 44,
    height: 44,
    marginTop: 6,
    marginBottom: 6,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  text: {
    flex: 1,
    fontSize: 20,
    textAlign: "center",
    fontFamily: "AvenirNext-Medium",
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 30,
    color: Colors.P_80,
    letterSpacing: 0,
  },
  textScrollView: {
    height: "100%",
    marginBottom: 8,
  },
});
