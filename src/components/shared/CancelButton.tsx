import React, { useState } from "react";
import { Text, StyleSheet } from "react-native";
import { Colors } from "../../utils/Colors";
import CustomModal from "./CustomModal";

export default ({ navigation }: any) => {
  const [cancelModalVisible, setCancelModalVisible] = useState<boolean>(false);

  return (
    <>
      <Text onPress={() => setCancelModalVisible(true)} style={styles.text}>
        Cancel
      </Text>
      <CustomModal
        title="Cancel report?"
        text="All data will be lost. Are you sure you want to cancel your report?"
        visible={cancelModalVisible}
        handleOnRequestClose={() => {}}
        buttons={[
          {
            text: "Yes",
            handlePress: () => {
              setCancelModalVisible(false);
              navigation.reset({ index: 0, routes: [{ name: "Landing" }] });
            },
          },
          { text: "No", handlePress: () => setCancelModalVisible(false) },
        ]}
      />
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    marginRight: 16,
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    color: Colors.P_80,
  },
});
