import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  useWindowDimensions,
} from "react-native";
import { DataController } from "../../controllers/DataController";
import { ReportProgressProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import { UtilFunctions } from "../../utils/UtilFunctions";

export default ({ title, pageNumber }: ReportProgressProps) => {
  const numberOfPages = DataController.getData().reportingPages.length;

  const progressComplete = (pageNumber / numberOfPages) * 100;

  return (
    <View style={styles.mainContainer}>
      <View
        style={
          UtilFunctions.isSmallViewport()
            ? styles.smallDisplay
            : styles.largeDisplay
        }
      >
        <View style={styles.titleAndPageNumber}>
          <Text style={styles.title}>{title}</Text>
          <Text
            style={styles.pageNumber}
          >{`${pageNumber} of ${numberOfPages}`}</Text>
        </View>
        <View style={styles.progressBar}>
          <View
            style={[
              styles.progressBarComplete,
              { width: `${progressComplete}%` },
            ]}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    alignItems: "center",
    paddingTop: 8,
    paddingBottom: 16,
    backgroundColor: Colors.P_20,
  },
  smallDisplay: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  largeDisplay: {
    width: 624,
    paddingLeft: 5,
    paddingRight: 5,
  },
  pageNumber: {
    fontFamily: "AvenirNext-Bold",
    fontSize: 13,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.P_50,
  },
  progressBar: {
    width: "100%",
    height: 8,
    borderRadius: 4,
    backgroundColor: Colors.PROGRESS_INCOMPLETE,
  },
  progressBarComplete: {
    height: "100%",
    borderRadius: 4,
    backgroundColor: Colors.PROGRESS_COMPLETE,
  },
  title: {
    fontFamily: "AvenirNext-Bold",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: -0.1,
    color: Colors.P_80,
  },
  titleAndPageNumber: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 8,
  },
});
