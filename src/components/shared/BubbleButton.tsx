import React from "react";
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  ViewStyle,
  TextStyle,
  ActivityIndicator,
} from "react-native";
import { BubbleButtonProps, BubbleButtonSize } from "../../domain/Types";
import { Colors } from "../../utils/Colors";

export default ({
  text,
  buttonSize = BubbleButtonSize.Small,
  textColour,
  borderColour,
  marginStyle,
  handlePress,
  disabled,
  showActivityIndicator = false,
}: BubbleButtonProps) => {
  const setButtonSize = (buttonSize: BubbleButtonSize): ViewStyle => {
    switch (buttonSize) {
      case BubbleButtonSize.Small:
        return styles.sizeSmall;
      case BubbleButtonSize.Large:
        return styles.sizeLarge;
    }
  };
  const setButtonTextSize = (buttonSize: BubbleButtonSize): TextStyle => {
    switch (buttonSize) {
      case BubbleButtonSize.Small:
        return styles.smallText;
      case BubbleButtonSize.Large:
        return styles.largeText;
    }
  };
  return (
    <TouchableHighlight
      disabled={disabled}
      underlayColor={Colors.P_10}
      onPress={handlePress}
      style={[
        disabled ? styles.disabled : null,
        styles.container,
        setButtonSize(buttonSize),
        borderColour,
        marginStyle,
      ]}
    >
      {showActivityIndicator ? (
        <ActivityIndicator color="#999999" />
      ) : (
        <Text
          allowFontScaling={false}
          style={[styles.text, textColour, setButtonTextSize(buttonSize)]}
        >
          {text}
        </Text>
      )}
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: Colors.P_80,
    justifyContent: "center",
    alignItems: "center",
  },
  disabled: {
    opacity: 0.4,
  },
  sizeSmall: {
    borderRadius: 44,
    width: 88,
    height: 88,
  },
  sizeLarge: {
    borderRadius: 60,
    width: 120,
    height: 120,
  },
  text: {
    color: Colors.P_80,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "center",
    paddingTop: 2,
    fontFamily: "AvenirNext-Bold",
    letterSpacing: -0.1,
  },
  smallText: {
    fontSize: 17,
  },
  largeText: {
    fontSize: 20,
  },
});
