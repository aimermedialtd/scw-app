import React from "react";
import { Text, View, StyleSheet, TouchableHighlight, Modal } from "react-native";
import { ModalProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { UtilFunctions } from "../../utils/UtilFunctions";

export default ({ title, text, visible, buttons, handleOnRequestClose }: ModalProps) => {
  return (
    <Modal animationType="fade" transparent={true} visible={visible} onRequestClose={handleOnRequestClose}>
      <View style={styles.container}>
        <View
          style={[
            styles.modal,
            {
              width: UtilFunctions.isSmallViewport() ? "80%" : "50%",
            },
          ]}
        >
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.text}>{text}</Text>
          {buttons.map((value, index, array) => {
            return (
              <View key={index} style={{ width: "100%" }}>
                <View
                  style={{
                    height: 1,
                    backgroundColor: "#d1d1d6",
                    width: "100%",
                  }}
                />
                <TouchableHighlight
                  underlayColor={Colors.P_20}
                  style={[
                    {
                      // backgroundColor: "blue",
                      paddingVertical: 10,
                    },
                    array.length === index + 1
                      ? {
                          borderBottomRightRadius: 20,
                          borderBottomLeftRadius: 20,
                        }
                      : null,
                  ]}
                  onPress={value.handlePress}
                >
                  <Text style={[styles.buttonText, value.fontStyle]}>{value.text}</Text>
                </TouchableHighlight>
              </View>
            );
          })}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22,
    backgroundColor: "rgba(0, 0, 0, 0.6)",
  },
  title: {
    marginTop: 16,
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 20,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0.1,
    textAlign: "center",
    color: Colors.P_80,
  },
  text: {
    marginTop: 4,
    marginBottom: 16,
    marginHorizontal: 15,
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    textAlign: "center",
    color: Colors.P_80,
  },
  buttonText: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 17,
    // fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: -0.1,
    textAlign: "center",
    color: Colors.CTA_BLUE,
  },
  modal: {
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
