import React from "react";
import { Text, View, StyleSheet, Image, useWindowDimensions } from "react-native";
import { WarningBoxProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import { UtilFunctions } from "../../utils/UtilFunctions";

export default ({ text, customStyling }: WarningBoxProps) => {
  return (
    <View
      style={[styles.warningBoxContainer, customStyling, { width: UtilFunctions.isSmallViewport() ? "100%" : 624 }]}
    >
      <Image style={styles.warningBoxIcon} source={require("../../../assets/images/alert.png")} />
      <View style={styles.warningBox}>
        <Text style={styles.warningBoxText}>{text}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  warningBox: {
    width: "100%",
    paddingTop: 32,
    paddingBottom: 20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 10,
    backgroundColor: Colors.WHITE,
  },
  warningBoxContainer: {
    alignItems: "center",
  },
  warningBoxIcon: {
    width: 44,
    height: 44,
    position: "absolute",
    top: -22, // should be half the icon's height
    zIndex: 1,
    marginBottom: 0,
  },
  warningBoxText: {
    fontSize: 15,
    fontWeight: "600",
    fontStyle: "normal",
    fontFamily: "AvenirNext-DemiBold",
    lineHeight: 20,
    letterSpacing: -0.1,
    textAlign: "center",
    color: Colors.P_80,
  },
});
