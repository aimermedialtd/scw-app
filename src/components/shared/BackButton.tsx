import React from "react";
import { Text, StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Colors } from "../../utils/Colors";

export default ({ handlePress }: any) => {
  return (
    <TouchableOpacity onPress={handlePress} style={styles.mainContainer}>
      <>
        <Image
          style={styles.image}
          source={require("../../../assets/images/btn_back.png")}
        />
        <Text style={styles.text}>Back</Text>
      </>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 5,
    // backgroundColor: "blue",
  },
  text: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 17,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.1,
    color: Colors.P_80,
    // backgroundColor: "blue",
  },
  image: {
    width: 44,
    height: 44,
    marginRight: -5,
  },
});
