import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import { DontKnowPillButtonProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";

export default ({ selected, handlePress }: DontKnowPillButtonProps) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={handlePress}
      style={[
        styles.container,
        selected ? styles.selectedButton : styles.defaultButton,
      ]}
    >
      <Text
        style={[
          styles.text,
          selected ? styles.selectedText : styles.defaultText,
        ]}
      >
        {"DON'T KNOW"}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 18,
    borderStyle: "solid",
  },
  defaultButton: {
    backgroundColor: Colors.WHITE,
  },
  defaultText: {
    color: Colors.SECONDARY_ORANGE,
  },
  selectedButton: {
    backgroundColor: Colors.P_30,
  },
  selectedText: {
    color: Colors.WHITE,
  },
  text: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 15,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: -0.1,
    textAlign: "center",
  },
});
