import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  useWindowDimensions,
} from "react-native";
import HTML from "react-native-render-html";
import { CompletePageTextBodyProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import { UtilFunctions } from "../../utils/UtilFunctions";

export default ({ text }: CompletePageTextBodyProps) => {
  const tagStyles = {
    b: styles.boldText,
    p: styles.paragraphText,
    a: styles.link,
  };

  return (
    <View
      style={{
        width: UtilFunctions.isSmallViewport() ? "100%" : 624,
        alignSelf: "center",
      }}
    >
      <HTML
        contentWidth={Dimensions.get("window").width}
        tagsStyles={tagStyles}
        source={{ html: text || "<p></p>" }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  boldText: {
    fontWeight: "bold",
  },
  paragraphText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    textAlign: "center",
    marginTop: 8,
    color: Colors.P_80,
  },
  link: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    textAlign: "center",
    marginTop: 8,
    color: "blue",
  },
});
