import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FlatButtonProps } from "../../domain/Types";
import { Text, StyleSheet } from "react-native";

export default ({ text, handlePress, filled = false }: FlatButtonProps) => {
  return (
    <TouchableOpacity onPress={handlePress} style={[styles.container, { backgroundColor: filled ? "rgb(91,63,0)" : "transparent" }]}>
      <Text style={[styles.text, { color: filled ? "white" : "black" }]}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    justifyContent: "center",
    alignItems: "center",
    width: 275,
    height: 50,
    borderRadius: 25,
    borderColor: "rgb(91,63,0)",
    borderStyle: "solid",
  },
  text: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 22,
    // fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 30,
    letterSpacing: 0.2,
  },
});
