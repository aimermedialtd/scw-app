import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { PillButtonProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";

export default ({
  text,
  selected,
  largeMinWidth,
  handlePress,
}: PillButtonProps) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={handlePress}
      style={[
        styles.container,
        selected ? styles.selectedButton : styles.defaultButton,
        { minWidth: largeMinWidth ? 110 : 65 },
      ]}
    >
      <Text
        allowFontScaling={false}
        style={[
          styles.text,
          selected ? styles.selectedText : styles.defaultText,
        ]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 23,
    margin: 6,
    height: 46,
    padding: 13,
    justifyContent: "center",
    alignItems: "center",
    borderStyle: "solid",
    borderWidth: 2,
  },
  defaultButton: {
    borderColor: Colors.SECONDARY_ORANGE,
    backgroundColor: Colors.WHITE,
  },
  selectedButton: {
    borderColor: Colors.P_30,
    backgroundColor: Colors.P_30,
  },
  defaultText: {
    color: Colors.SECONDARY_ORANGE,
  },
  selectedText: {
    color: Colors.WHITE,
  },
  text: {
    fontFamily: "AvenirNext-Bold",
    height: 24,
    fontSize: 17,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: -0.1,
    textAlign: "center",
  },
});
