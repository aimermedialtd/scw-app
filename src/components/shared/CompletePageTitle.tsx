import React from "react";
import { Text, StyleSheet } from "react-native";
import { CompletePageTitleProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";

export default ({ text }: CompletePageTitleProps) => {
  return <Text style={styles.text}>{text}</Text>;
};

const styles = StyleSheet.create({
  text: {
    fontFamily: "AvenirNext-DemiBold",
    marginTop: 16,
    fontSize: 22,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: "center",
    color: Colors.P_80,
  },
});
