import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, StatusBar, View, Platform, ActivityIndicator } from "react-native";
import { DataController } from "../controllers/DataController";
import { BubbleButtonSize } from "../domain/Types";
import BubbleButton from "./shared/BubbleButton";
import WarningBox from "./shared/WarningBox";
import { TouchableOpacity } from "react-native-gesture-handler";
import CustomModal from "./shared/CustomModal";
import { UtilFunctions } from "../utils/UtilFunctions";
import { Constants } from "../utils/Constants";
import * as ExpoLocation from "expo-location";
import { Colors } from "../utils/Colors";
import AsyncStorage from "@react-native-async-storage/async-storage";

// not type safe way of taking in arguments but I'm forced to do this to accept new pages
export default ({ navigation, route }: any) => {
  const [requestingPermissions, setRequestingPermissions] = useState(true);
  const [startModalVisible, setStartModeModalVisible] = useState(false);
  const [demoModalVisible, setDemoModalVisible] = useState(false);
  const [locationPermission, setLocationPermission] = useState(false);

  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor(Colors.P_20, true);
  }

  useEffect(() => {
    requestLocationPermission();
  }, []);

  const requestLocationPermission = async () => {
    try {
      const { status } = await ExpoLocation.requestForegroundPermissionsAsync();
      if (status === "granted") {
        setLocationPermission(true);

        const firstLaunch = DataController.isFirstLaunch();
        if (firstLaunch) {
          await getCounty();
        }
      }
    } catch (error) {
      console.log("Failed to request location permission on landing page. Error: ", error);
    } finally {
      setRequestingPermissions(false);
    }
  };

  const getCounty = async () => {
    const location = await ExpoLocation.getCurrentPositionAsync({
      accuracy: ExpoLocation.Accuracy.Low,
    });

    const res = await fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${location.coords.latitude},${location.coords.longitude}&key=${Constants.MAPS_API_KEY}`
    );

    const json = await res.json();

    let temp = json.results[0];
    json.results.forEach(item => {
      if (item.address_components.length > temp.address_components.length) {
        temp = item;
      }
    });

    const countyObj = temp.address_components.find(item => item.types.includes("administrative_area_level_2"));

    await DataController.sendCounty(countyObj.long_name);
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.openDrawer()} disabled={requestingPermissions}>
          <Image
            style={{
              marginLeft: 5,
              width: 44,
              height: 44,
              opacity: requestingPermissions ? 0.4 : 1,
            }}
            source={require("../../assets/images/menu.png")}
          />
        </TouchableOpacity>
      ),
    });
  });

  const handleStartClick = async () => {
    const res = await AsyncStorage.getItem(Constants.STORAGE_KEY_DEMO_MODE_ACKNOWLEDGED);
    if (res === null) {
      setStartModeModalVisible(true);
    } else {
      beginReport(false);
    }
  };

  const beginReport = async (demoMode: boolean) => {
    DataController.resetReport();
    DataController.setDemoMode(demoMode);

    navigation.navigate(DataController.getData().reportingPages[0].name, {
      page: DataController.getData().reportingPages[0],
      locationPermissionIsGranted: locationPermission,
    });
  };

  let smallViewport = false;
  if (UtilFunctions.isSmallViewport()) {
    smallViewport = true;
  } else {
    smallViewport = false;
  }

  if (requestingPermissions) {
    return <ActivityIndicator size="large" color="#999999" style={{ flex: 1, backgroundColor: Colors.P_20 }} />;
  }

  return (
    <View style={styles.mainContainer}>
      <View
        style={{
          alignItems: "center",
        }}
      >
        <Text style={styles.startPrompt}>At a hand car wash?</Text>
        <BubbleButton
          text="START"
          buttonSize={BubbleButtonSize.Large}
          borderColour={styles.startButtonBorder}
          textColour={styles.startButtonText}
          handlePress={handleStartClick}
        />
        <Text style={styles.demoPrompt}>Or want to try the app first?</Text>
        <BubbleButton
          text="DEMO"
          buttonSize={BubbleButtonSize.Small}
          borderColour={styles.demoButtonBorder}
          textColour={styles.demoButtonText}
          handlePress={() => setDemoModalVisible(true)}
        />
      </View>
      <WarningBox
        text={
          smallViewport ? Constants.STR_WARNING_BOX_TEXT_LANDING_SMALL : Constants.STR_WARNING_BOX_TEXT_LANDING_LARGE
        }
        customStyling={styles.warningBox}
      />
      <CustomModal
        title="Hang on!"
        text="You haven't tried the Demo yet. Your answers will be submitted and analysed. Would you like to try out the demo first?"
        visible={startModalVisible}
        buttons={[
          {
            text: "Try demo",
            handlePress: () => {
              setStartModeModalVisible(false);
              setDemoModalVisible(true);
            },
          },
          {
            text: "Skip demo",
            handlePress: async () => {
              setStartModeModalVisible(false);
              await AsyncStorage.setItem(Constants.STORAGE_KEY_DEMO_MODE_ACKNOWLEDGED, JSON.stringify(true));
              beginReport(false);
            },
          },
        ]}
        handleOnRequestClose={() => setStartModeModalVisible(false)}
      />
      <CustomModal
        title="Demo mode"
        text="You can view and answer all quetions but your answers will not be submitted."
        visible={demoModalVisible}
        buttons={[
          {
            text: "Got it!",
            handlePress: async () => {
              setDemoModalVisible(false);
              await AsyncStorage.setItem(Constants.STORAGE_KEY_DEMO_MODE_ACKNOWLEDGED, JSON.stringify(true));
              beginReport(true);
            },
          },
        ]}
        handleOnRequestClose={() => setDemoModalVisible(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.P_20,
    alignItems: "center",
    paddingRight: 20,
    paddingLeft: 20,
  },
  demoButtonBorder: {
    borderColor: Colors.P_60,
  },
  demoButtonText: {
    color: Colors.P_60,
  },
  demoPrompt: {
    marginTop: 25,
    marginBottom: 16,
    fontFamily: "AvenirNext-Medium",
    fontSize: 20,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0.1,
    color: Colors.P_60,
  },
  startButtonBorder: {
    borderColor: Colors.P_70,
  },
  startButtonText: {
    color: Colors.P_80,
  },
  startPrompt: {
    marginTop: 25,
    marginBottom: 16,
    fontWeight: "600",
    fontSize: 22,
    fontFamily: "AvenirNext-DemiBold",
    fontStyle: "normal",
    lineHeight: 30,
    letterSpacing: 0,
    color: Colors.P_80,
  },
  warningBox: {
    marginTop: "auto",
    marginBottom: 32,
  },
});
