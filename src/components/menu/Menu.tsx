import { DrawerContentComponentProps } from "@react-navigation/drawer";
import React, { useEffect, useRef, useState } from "react";
import {
  Text,
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  Platform,
  Share,
  Linking,
} from "react-native";
import { EventRegister } from "react-native-event-listeners";
import { FlatList, ScrollView, TouchableHighlight, TouchableOpacity } from "react-native-gesture-handler";
import HTML from "react-native-render-html";
import { DataController } from "../../controllers/DataController";
import { MenuItem, MenuItemProps } from "../../domain/Types";
import { Colors } from "../../utils/Colors";
import { Constants } from "../../utils/Constants";
import { UtilFunctions } from "../../utils/UtilFunctions";
import BackButton from "../shared/BackButton";

const MenuItemButton = ({ title, text, handleMenuItemClick }: MenuItemProps) => {
  return (
    <TouchableHighlight underlayColor={Colors.P_10} style={styles.menuItem} onPress={() => handleMenuItemClick()}>
      <View style={styles.menuItemContent}>
        <Text style={styles.text}>{title}</Text>
        <Image style={styles.image} source={require("../../../assets/images/arrow.png")} />
      </View>
    </TouchableHighlight>
  );
};

export default ({ navigation, state }: DrawerContentComponentProps) => {
  const menuItemAbout = DataController.getMenuItem(Constants.ABOUT_MENU_ITEM);
  const menuItemHow = DataController.getMenuItem(Constants.HOW_MY_DATA_IS_USED_MENU_ITEM);

  const flatListRef = useRef<FlatList<any>>(null);
  const [itemSelectionContent, setItemSelectionContent] = useState<MenuItem>({
    title: "",
    text: "",
  });

  useEffect(() => {
    EventRegister.addEventListener("learnMore", params => {
      setItemSelectionContent({
        title: menuItemHow.title,
        text: menuItemHow.text,
      });
      flatListRef.current?.scrollToIndex({ index: 1 });
      navigation.openDrawer();
    });

    return () => {
      EventRegister.removeEventListener("learnMore");
    };
  }, []);

  const tagStyles = {
    header: styles.htmlHeader,
    p: styles.p,
    a: styles.link,
    b: styles.boldText,
  };

  const handleShareBtn = () => {
    try {
      if (Platform.OS === "web") {
        Linking.openURL(
          `mailto:?subject=Safe%20Car%20Wash&body=Help%20end%20modern%20slavery%20with%20the%20Safe%20Car%20Wash%20app:%0D%0A%0D%0A${Constants.URL_APP_INFO_PAGE}`
        );
      } else {
        Share.share({
          message:
            Platform.OS === "ios"
              ? `Help end modern slavery with the Safe Car Wash app.`
              : `Help end modern slavery with the Safe Car Wash app. ${Constants.URL_APP_INFO_PAGE}`,
          url: Constants.URL_APP_INFO_PAGE,
        });
      }
    } catch (error) {
      console.log("Failed to share app. Error: ", error);
    }
  };

  const menuContentArray = [
    // menu item buttons
    <View
      style={{
        width: UtilFunctions.isSmallViewport() ? Dimensions.get("window").width : 375,
      }}
    >
      <View style={styles.menuItemButtonsContainer}>
        <MenuItemButton
          title={menuItemAbout.title}
          text={menuItemAbout.text}
          handleMenuItemClick={() => {
            flatListRef.current?.scrollToIndex({ index: 1 });
            setItemSelectionContent({
              title: menuItemAbout.title,
              text: menuItemAbout.text,
            });
          }}
        />
        <View style={styles.divider} />
        <MenuItemButton
          title={menuItemHow.title}
          text={menuItemHow.text}
          handleMenuItemClick={() => {
            flatListRef.current?.scrollToIndex({ index: 1 });
            setItemSelectionContent({
              title: menuItemHow.title,
              text: menuItemHow.text,
            });
          }}
        />
        <View style={styles.divider} />
        <MenuItemButton
          title="How the app works"
          handleMenuItemClick={() => {
            navigation.reset({ index: 0, routes: [{ name: "Onboarding" }] });
            navigation.closeDrawer();
          }}
        />
      </View>
      <View style={styles.shareBtnContainer}>
        <TouchableOpacity onPress={handleShareBtn}>
          <Text style={styles.shareText}>Share the app</Text>
        </TouchableOpacity>
      </View>
    </View>,
    // menu content from menu item buttons
    <View style={{ backgroundColor: Colors.WHITE }}>
      <ScrollView
        contentContainerStyle={{ paddingBottom: 16, paddingTop: 5 }}
        showsVerticalScrollIndicator={false}
        style={{
          // backgroundColor: "red",
          paddingHorizontal: 20,
          width: UtilFunctions.isSmallViewport() ? Dimensions.get("window").width : 375,
        }}
      >
        <HTML
          contentWidth={Dimensions.get("window").width}
          tagsStyles={tagStyles}
          source={{ html: itemSelectionContent?.text || "<p></p>" }}
        />
      </ScrollView>
    </View>,
  ];

  return (
    <SafeAreaView style={styles.mainContainer}>
      {/* header */}
      <View
        style={{
          flexDirection: "row",
          //   backgroundColor: "blue",
          justifyContent: itemSelectionContent.title === "" ? "flex-end" : "space-between",
        }}
      >
        {/* back button */}
        {itemSelectionContent.title !== "" && (
          <BackButton
            handlePress={() => {
              flatListRef.current?.scrollToIndex({ index: 0 });
              setItemSelectionContent({ ...itemSelectionContent, title: "" });
            }}
          />
        )}
        {/* logo or title */}
        {itemSelectionContent.title === "" ? (
          <View
            style={{
              width: "100%",
              zIndex: -1,
              position: "absolute",
              alignItems: "center",
            }}
          >
            <Image style={{ width: 44, height: 44 }} source={require("../../../assets/images/logo.png")} />
          </View>
        ) : (
          <Text style={styles.headerTitle}>{itemSelectionContent.title}</Text>
        )}
        {/* close button */}
        <TouchableOpacity
          onPress={() => {
            navigation.closeDrawer();
            flatListRef.current?.scrollToIndex({ index: 0 });
            setItemSelectionContent({ ...itemSelectionContent, title: "" });
          }}
        >
          <Image
            style={{
              width: 44,
              height: 44,
            }}
            source={require("../../../assets/images/close.png")}
          />
        </TouchableOpacity>
      </View>
      {/* menu content */}
      <FlatList
        // style={{ backgroundColor: "blue" }}
        horizontal
        ref={flatListRef}
        scrollEnabled={false}
        data={menuContentArray}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({ item }) => {
          return item;
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.P_20,
    paddingTop: StatusBar.currentHeight,
  },
  menuItemButtonsContainer: {
    flex: 1,
  },
  text: {
    marginLeft: 20,
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 20,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0.1,
    color: Colors.P_80,
  },
  headerTitle: {
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 17,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: -0.1,
    textAlign: "center",
    color: Colors.P_80,
    textAlignVertical: "center",
    alignSelf: "center",
    width: "100%",
    position: "absolute",
    zIndex: -1,
  },
  shareText: {
    textAlign: "center",
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 15,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: -0.1,
    color: Colors.P_70,
  },
  divider: {
    backgroundColor: Colors.P_35,
    height: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  image: {
    width: 44,
    height: 44,
    marginRight: 20,
  },
  shareBtnContainer: {
    alignSelf: "center",
    marginTop: "auto",
    marginBottom: 32,
  },
  p: {
    marginTop: 6,
    marginBottom: 6,
    fontFamily: "AvenirNext-Regular",
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 26,
    letterSpacing: -0.2,
    color: Colors.P_80,
  },
  htmlHeader: {
    marginTop: 16,
    marginBottom: 8,
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 20,
    fontWeight: "600",
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0.1,
    color: Colors.P_80,
  },
  menuItem: {
    justifyContent: "center",
    height: 60,
    width: "100%",
  },
  menuItemContent: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  link: {
    fontSize: 17,
  },
  boldText: {
    fontWeight: "bold",
  },
});
