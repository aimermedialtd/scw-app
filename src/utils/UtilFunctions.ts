import * as Font from "expo-font";
import { DataController } from "../controllers/DataController";
import { useMediaQuery } from "react-responsive";
import { Constants } from "./Constants";
import { useWindowDimensions } from "react-native";
import { Question } from "../domain/Types";

export namespace UtilFunctions {
  export const loadFonts = () => {
    return Font.loadAsync({
      "AvenirNext-Bold": require("../../assets/fonts/AvenirNext-Bold.ttf"),
      "AvenirNext-BoldItalic": require("../../assets/fonts/AvenirNext-BoldItalic.ttf"),
      "AvenirNext-DemiBold": require("../../assets/fonts/AvenirNext-DemiBold.ttf"),
      "AvenirNext-DemiBoldItalic": require("../../assets/fonts/AvenirNext-DemiBoldItalic.ttf"),
      "AvenirNext-Heavy": require("../../assets/fonts/AvenirNext-Heavy.ttf"),
      "AvenirNext-HeavyItalic": require("../../assets/fonts/AvenirNext-HeavyItalic.ttf"),
      "AvenirNext-Italic": require("../../assets/fonts/AvenirNext-Italic.ttf"),
      "AvenirNext-Medium": require("../../assets/fonts/AvenirNext-Medium.ttf"),
      "AvenirNext-MediumItalic": require("../../assets/fonts/AvenirNext-MediumItalic.ttf"),
      "AvenirNext-Regular": require("../../assets/fonts/AvenirNext-Regular.ttf"),
      "AvenirNext-UltraLight": require("../../assets/fonts/AvenirNext-UltraLight.ttf"),
      "AvenirNext-UltraLightItalic": require("../../assets/fonts/AvenirNext-UltraLightItalic.ttf"),
    });
  };

  export const isSmallViewport = () => {
    return useWindowDimensions().width <= Constants.SMALL_DISPLAY_WIDTH_THRESHOLD;
  };

  export const getMiscellaneousImage = (imageId: string) => {
    return DataController.getData().miscellaneousImages.find(img => img.id === imageId);
  };

  export const isEmailValid = (email: string) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.trim());
  };

  // checks if question is number 9/12/13 (used to display an extra text input field)
  export const checkIfQuestionMatchesSet = (question: Question) => {
    return (
      question.id === Constants.OVER_CONTROLLER_OR_INTIMIDATING_ID ||
      question.id === Constants.UNDER_18S_ID ||
      question.id === Constants.UNDER_16S_ID ||
      question.id === Constants.LIVING_ONSITE_QUESTION_ID ||
      question.id === Constants.CLOTHING_QUESTION_ID
    );
  };
}
