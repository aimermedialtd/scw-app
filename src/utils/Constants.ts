export namespace Constants {
  // question ids
  export const NAME_QUESTION_ID = "recJ3Z7T1x4KhDHv1";
  export const PRICE_QUESTION_ID = "recJ1v2Shb5LCT90T";
  export const WASH_TYPE_QUESTION_ID = "recX0alW1k7P5Iezj";
  export const SIZE_QUESTION_ID = "recvPk9DdpQnIIcAP";
  export const OVER_CONTROLLER_OR_INTIMIDATING_ID = "recrZMJx3m7Xk9fyX"; // question 9
  export const LIVING_ONSITE_QUESTION_ID = "recJMCGb26vtp6vPP";
  export const CLOTHING_QUESTION_ID = "recRJ8v4nHioSdIv6";
  export const UNDER_18S_ID = "receK2CorweD6fnlH"; // question 12
  export const UNDER_16S_ID = "recXwyuunXwvCXWKo"; // question 13

  // menu item identifiers
  export const ABOUT_MENU_ITEM = "About";
  export const HOW_MY_DATA_IS_USED_MENU_ITEM = "How my data is used";

  // address field identifiers
  export const POSTCODE_ID = "recgPNtTPkyuf9JVc";

  // threshold for detecting if a report should be submitted the modern slavery helpline
  export const MODERN_SLAVERY_SCORE_THRESHOLD = 2.5;

  // threshold for differentiaing between ipad/desktop and phone displays
  export const SMALL_DISPLAY_WIDTH_THRESHOLD = 780;

  // keys for reading/writing in Async Storage
  export const STORAGE_KEY_DEMO_MODE_ACKNOWLEDGED = "demoModeIsAcknowledged";
  export const STORAGE_KEY_FIRST_LAUNCH = "firstLaunch";
  export const STORAGE_KEYS = [STORAGE_KEY_DEMO_MODE_ACKNOWLEDGED, STORAGE_KEY_FIRST_LAUNCH];

  // Regular expressions
  export const REG_EX_P_TAG = /<p.*?>\s?[\s\S]+?<\/p>/gim;
  export const REG_EX_LI_TAG = /<li.*?>\s?[\s\S]+?<\/li>/gim;

  // URL for retrieving data from Amazon S3
  export const URL_AMAZON_S3 = "https://safecarwash.s3.amazonaws.com/ATData.json";

  // API key for using google maps
  export const MAPS_API_KEY = "AIzaSyCThB6eQuGL8_qUbDXIFykwo6NQklP81kw";

  // The phone number for contacting Modern Slavery should it be deemed necessary.
  export const MODERN_SLAVERY_PHONE_NUMBER = "08000121700";

  // API URLs for getting/posting report data
  export const ENDPOINT_SEND_REPORT_MSH = "https://api.modernslaveryhelpline.org/SubmitReport";
  export const ENDPOINT_SEND_REPORT_AIMER = "https://scw.aimernginx.co.uk/scw/app/sendReport";
  export const ENDPOINT_MARK_REPORT_AS_SENT = "https://scw.aimernginx.co.uk/scw/app/markSentToMSH";
  export const ENDPOINT_SEND_COUNTY = "https://scw.aimernginx.co.uk/scw/app/reportCounty";

  // Link to an information page on the app from Clewer
  export const URL_APP_INFO_PAGE = "https://theclewerinitiative.org/campaigns/safe-car-wash";

  // Strings
  export const STR_WARNING_BOX_TEXT_COMPLETE =
    "FOR THEIR SAFETY AND YOURS, PLEASE DO NOT ATTEMPT TO ENGAGE WORKERS OR STAFF YOURSELF";
  export const STR_WARNING_BOX_TEXT_LANDING_SMALL = "IF SOMEONE IS IN IMMEDIATE DANGER PLEASE CONTACT THE POLICE";
  export const STR_WARNING_BOX_TEXT_LANDING_LARGE = "IF SOMEONE IS IN IMMEDIATE DANGER\n PLEASE CONTACT THE POLICE";

  // reporting api strings
  export const MSH_REPORT_ENQUIRY_TYPE = "I think I have witnessed examples of modern slavery or human trafficking";
  export const AIMER_REPORT_AUTH_KEY = "5cf9dfd5-3449-485e-b5ae-70a60e997864";

  // flags for sending report as tests
  export const SUBMIT_REPORT_TO_MSH_AS_TEST = false;
  export const SUBMIT_REPORT_TO_AIMER_AS_TEST = false;

  // channel used for reporting to MSH
  export const MSH_REPORTING_CHANNEL = "carwash";
}
