import { StyleSheet } from "react-native";
import { Colors } from "./Colors";

export default StyleSheet.create({
  textInput: {
    width: "100%",
    borderColor: Colors.INPUT_BORDER,
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: -0.2,
    color: Colors.P_80,
    fontFamily: "AvenirNext-Regular",
    borderWidth: 1,
    borderRadius: 8,
    padding: 10,
    borderStyle: "solid",
    height: 44,
  },
  textInputMultiline: {
    width: "100%",
    backgroundColor: Colors.GREY_30,
    borderColor: Colors.INPUT_BORDER,
    fontSize: 17,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: -0.2,
    color: Colors.P_80,
    fontFamily: "AvenirNext-Regular",
    borderWidth: 1,
    borderRadius: 8,
    padding: 10,
    borderStyle: "solid",
  },
});
