import AsyncStorage from "@react-native-async-storage/async-storage";
import * as FileSystem from "expo-file-system";
import publicIP from "react-native-public-ip";
import { Platform } from "react-native";
import {
  Address,
  AddressField,
  AirtableData,
  Image,
  CompletedPage,
  MenuItem,
  OnboardingPage,
  ReportingPage,
  Choice,
  Question,
  Location,
  MSHFormData,
} from "../domain/Types";
import { Constants } from "../utils/Constants";

export namespace DataController {
  let data: AirtableData;
  let address: Address = {
    streetNumber: "",
    route: "",
    city: "",
    district: "",
    postalCode: "",
    region: "",
    country: "",
  };
  let demoMode: boolean = false;
  let location: Location;
  let mshFormData: MSHFormData;
  let reportID: string;
  let firstLaunch: boolean;

  export async function initData() {
    try {
      const localJson = require("../../assets/json/ATData.json");
      data = await mapJsonData(localJson);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async function mapJsonData(jsonData: any): Promise<AirtableData> {
    // get reporting pages
    let reportingPages: ReportingPage[] = [];
    for (const page of jsonData.reportingPages) {
      // get questions for page

      const tempQuestions = jsonData.questions.filter(q => q.fields.Page[0] === page.id);

      // console.log("temp questions: ", tempQuestions);
      const questions = await mapQuestions(tempQuestions);

      reportingPages.push({
        id: page.id,
        pageNumber: page.fields.PageNumber,
        name: page.fields.Name,
        questions: questions,
        lastModifiedTime: page.fields.LastModifiedTime,
      });
    }

    // get menu info
    let menuItems: MenuItem[] = [];
    jsonData.menuItems.forEach(item => {
      menuItems.push({
        title: item.fields.Title,
        text: item.fields.PageText,
      });
    });

    // get completed pages
    let completedPages: CompletedPage[] = [];
    jsonData.completedPages.forEach(item => {
      completedPages.push({
        name: item.fields.Name,
        title: item.fields.Title,
        text: item.fields.Text,
      });
    });

    // get address fields
    let addressFields: AddressField[] = [];
    jsonData.addressFields.forEach(item => {
      addressFields.push({
        id: item.id,
        text: item.fields.Text,
        required: item.fields.Required,
        answer: "",
      });
    });

    // get miscellaneous images
    let miscellaneousImages: Image[] = [];
    for (const item of jsonData.miscellaneousImages) {
      const imgObject = await generateLocalImageObject(item.fields.Image[0], item.id);
      miscellaneousImages.push(imgObject);
    }

    // get onboarding pages
    let onboardingPages: OnboardingPage[] = [];
    for (const item of jsonData.onboardingPages) {
      const imgObject = await generateLocalImageObject(item.fields.Image[0], item.id);
      onboardingPages.push({
        pageNumber: item.fields.PageNumber,
        image: imgObject,
        text: item.fields.Text,
      });
    }

    const mappedJsonData: AirtableData = {
      updated: jsonData.updated,
      reportingPages: reportingPages,
      menuItems: menuItems,
      completedPages: completedPages,
      addressFields: addressFields,
      miscellaneousImages: miscellaneousImages,
      onboardingPages: onboardingPages,
    };

    return mappedJsonData;
  }

  async function mapQuestions(tempQuestions: any[]) {
    let mappedQuestions: Question[] = [];

    for (const q of tempQuestions) {
      let choices: Choice[] = [];
      if (q.fields.Scoring) {
        const temp: { value: string; score: number }[] = JSON.parse(q.fields.Scoring);
        temp.forEach(item => {
          const newChoice: Choice = {
            value: item.value,
            score: item.score,
            selected: false,
          };
          choices.push(newChoice);
        });
      }
      let images: Image[] = [];
      if (q.fields.Images) {
        for (const item of q.fields.Images) {
          const result = await generateLocalImageObject(item, q.id);
          images.push(result);
        }
      }
      mappedQuestions.push({
        id: q.id,
        text: q.fields.Text,
        postamble: q.fields.Postamble,
        info: q.fields.Info,
        type: q.fields.Type,
        images: images,
        allowDoNotKnow: q.fields.AllowDoNotKnow,
        compoundScoring: q.fields.CompoundScoring,
        required: q.fields.Required,
        answered: false,
        doNotKnowSelected: false,
        choices: choices,
        answerText: "",
        lastModifiedTime: q.fields.LastModifiedTime,
      });
    }

    return mappedQuestions;
  }

  async function generateLocalImageObject(imageObj: any, id: string): Promise<Image> {
    // create and return an object pointing to the remote url for web because an image can't be saved to the device
    if (Platform.OS === "web") {
      return {
        id: id,
        uri: imageObj.url,
        width: imageObj.thumbnails.large.width,
        height: imageObj.thumbnails.large.height,
      };
    }

    // create an object pointing to the local location of the saved file
    const path = `${FileSystem.cacheDirectory}${imageObj.id}.png`;
    const localImg = await FileSystem.getInfoAsync(path);

    if (localImg.exists) {
      // file already exists so just return an object pointing to that file
      return {
        id: id,
        uri: localImg.uri,
        width: imageObj.thumbnails.large.width,
        height: imageObj.thumbnails.large.height,
      };
    } else {
      // download file first and then return object
      const newImg = await FileSystem.downloadAsync(imageObj.url, path);
      return {
        id: id,
        uri: newImg.uri,
        width: imageObj.thumbnails.large.width,
        height: imageObj.thumbnails.large.height,
      };
    }
  }

  export function setFirstLaunch(value: boolean) {
    firstLaunch = value;
  }

  export function isFirstLaunch() {
    return firstLaunch;
  }

  export async function clearStorage() {
    try {
      await AsyncStorage.multiRemove(Constants.STORAGE_KEYS);
    } catch (error) {
      console.log("Failed to clear Async Storage. Error: ", error);
    }
  }

  export function isModernSlaveryLikely(): boolean {
    return calculateTotalScore() >= Constants.MODERN_SLAVERY_SCORE_THRESHOLD;
  }

  export function calculateTotalScore() {
    let runningScore: number = 0;
    let compoundScore: number = 0;

    // calculate total score and get build questions array
    data.reportingPages.forEach(p => {
      p.questions.forEach(q => {
        q.choices.forEach(c => {
          if (c.selected) {
            runningScore += c.score;
          }
        });
      });
    });

    const matchFound = findCompoundScoringMatchingReportAnswers();
    if (matchFound) {
      compoundScore = matchFound.score;
    }

    const totalScore = compoundScore + runningScore;
    return totalScore;
  }

  export function resetReport() {
    data.reportingPages.forEach(p => {
      p.questions.forEach(q => {
        q.answered = false;
        q.answerText = "";
        q.choices.forEach(c => {
          c.selected = false;
        });
        q.doNotKnowSelected = false;
      });
    });
    data.addressFields.forEach(field => {
      field.answer = "";
    });
    address = {
      streetNumber: "",
      route: "",
      city: "",
      district: "",
      postalCode: "",
      region: "",
      country: "",
    };
    location = {
      latitude: 0,
      longitude: 0,
    };
    mshFormData = {
      firstName: "",
      lastName: "",
      agreeToSendData: false,
      canContact: false,
      canShareContact: false,
      email: "",
      phone: "",
    };
    reportID = "";
  }

  export function getAddress() {
    return address;
  }

  export function setAddress(newAddress: Address) {
    address = newAddress;
  }

  export function getQuestionForPage(pageNumber: number, questionIndex: number) {
    return data.reportingPages.find(p => p.pageNumber === pageNumber)!!.questions[questionIndex];
  }

  export function updateQuestionForPage(pageNumber: number, questionIndex: number, newQuestionState: Question) {
    data.reportingPages.find(p => p.pageNumber === pageNumber)!!.questions[questionIndex] = newQuestionState;
  }

  export function getMenuItem(title: string): MenuItem {
    const menuItem = data.menuItems.find(item => item.title === title);
    return menuItem!!;
  }

  export function isDemoMode(): boolean {
    return demoMode;
  }

  export function setDemoMode(value: boolean): void {
    demoMode = value;
  }

  export function getData(): AirtableData {
    return data;
  }

  export function setLocation(newLocation: Location | null) {
    location = newLocation;
  }

  export function setMSHFormData(data: MSHFormData) {
    mshFormData = data;
  }

  function findCompoundScoringMatchingReportAnswers() {
    const priceQ = getQuestionById(Constants.PRICE_QUESTION_ID);
    const selectedPrice = priceQ.choices.find(c => c.selected)!.value;
    const typeQ = getQuestionById(Constants.WASH_TYPE_QUESTION_ID);
    const selectedType = typeQ.choices.find(c => c.selected)!.value;
    const sizeQ = getQuestionById(Constants.SIZE_QUESTION_ID);
    const selectedSize = sizeQ.choices.find(c => c.selected)!.value;

    const compoundScoringConfigurations = JSON.parse(priceQ.compoundScoring!);

    const matchFound = compoundScoringConfigurations.find(
      config => config.price === selectedPrice && config.type === selectedType && config.size === selectedSize
    );

    if (matchFound) {
      return matchFound;
    } else {
      return null;
    }
  }

  // return a string of all the questions that contribute > 0 points to the total score plus the report id returned from the aimer report submission
  /* format:
    Report ID: <report-id>
    Q: <question> - A: <answer>
    Q: <question> - A: <answer>
  */
  /* example
    Report ID: 12345
    Q: Do there appear to be under 16s working at the site? - A: YES
  */
  export function buildDescriptionString() {
    let str = `Report ID: ${reportID === "" ? "Unavailable" : reportID}. Question/Answers: `;
    data.reportingPages.forEach((page, index) => {
      // ignore location page
      if (page.pageNumber != 1) {
        page.questions.forEach(question => {
          if (question.type === "SingleLineText" || question.type === "MultipleLineText") {
            str += `${"\n"}Q: ${question.text} - A: ${question.answerText}`;
          } else {
            // question must be of type CheckboxList, YES/NO, or MultipleChoice
            let answer = "";
            if (question.doNotKnowSelected) {
              answer = "DON'T KNOW";
            } else {
              const selectedChoices = question.choices.filter(c => c.selected);
              if (selectedChoices.length > 0) {
                if (selectedChoices.length === 1) {
                  answer = `${selectedChoices[0].value}`;
                  if (question.answerText !== "") answer += `. ${question.answerText}`;
                } else {
                  // multiple choices selected (chekcbox list)
                  selectedChoices.forEach((choice, index) => {
                    const extra = index + 1 === selectedChoices.length ? "" : ", ";
                    answer += `${choice.value}${extra}`;
                  });
                }
              }
            }
            str += `${"\n"}Q: ${question.text} - A: ${answer}`;
          }
        });
      }
    });

    return str;
  }

  /* format:
    for a gps enabled location
    <car-wash-name>, <street>, <city>, <county>, <postcode>, <region>, <country>
    for an address manually entered by user
    <car-wash-name>, <street>, <town>, <postcode>
  */
  /* example
    Happy Car Wash, Navarre Street, London, Greater London, E2 7JH, England, United Kingdom
  */
  export function buildLocationString() {
    let str = "";

    // add name if it exists
    const carWashName = getQuestionById(Constants.NAME_QUESTION_ID).answerText;
    if (carWashName !== "") {
      str += carWashName + ", ";
    }

    // add address/location
    if (data.addressFields.every(field => field.answer === "")) {
      // map/gps address

      // filter out empty address props
      const addressProps = [
        address.streetNumber,
        address.route,
        address.city,
        address.district,
        address.postalCode,
        address.region,
        address.country,
      ].filter(item => item !== "");
      addressProps.forEach((prop, index, array) => {
        str += prop;
        if (index != array.length - 1) str += ", ";
      });
    } else {
      // manual address
      data.addressFields.forEach((field, index, array) => {
        if (field.answer !== "") {
          str += field.answer;
          if (index != array.length - 1) str += ", ";
        }
      });
    }

    return str;
  }

  function getQuestionById(id: string) {
    const questions: Question[] = [];
    data.reportingPages.forEach(page => {
      page.questions.forEach(q => questions.push(q));
    });

    return questions.find(q => q.id === id)!;
  }

  function buildQuestionsAndAnswers() {
    const questionAndAnswers: { question: string; answer: string }[] = [];
    data.reportingPages.forEach(page => {
      // ignore location page
      if (page.pageNumber != 1) {
        page.questions.forEach(question => {
          if (question.type === "SingleLineText" || question.type === "MultipleLineText") {
            questionAndAnswers.push({
              question: question.text,
              answer: question.answerText,
            });
          } else {
            // question must be of type CheckboxList, YES/NO, or MultipleChoice
            let answer = "";
            if (question.doNotKnowSelected) {
              answer = "DON'T KNOW";
            } else {
              const selectedChoices = question.choices.filter(c => c.selected);
              if (selectedChoices.length > 0) {
                if (selectedChoices.length === 1) {
                  answer = `${selectedChoices[0].value}`;
                  if (question.answerText !== "") answer += `. ${question.answerText}`;
                } else {
                  // multiple choices selected (chekcbox list)
                  selectedChoices.forEach((choice, index) => {
                    const extra = index + 1 === selectedChoices.length ? "" : ", ";
                    answer += `${choice.value}${extra}`;
                  });
                }
              }
            }
            questionAndAnswers.push({
              question: question.text,
              answer: answer,
            });
          }
        });
      }
    });

    return questionAndAnswers;
  }

  export async function submitDataToModernSlaveryHelpline(): Promise<boolean> {
    try {
      const description = buildDescriptionString();
      const location = buildLocationString();
      const ipAddress = await publicIP();

      const postBody = {
        enquirytype: Constants.MSH_REPORT_ENQUIRY_TYPE, // required field
        description: description, // required field
        location: location,
        firstname: mshFormData.firstName,
        surname: mshFormData.lastName,
        can_contact: mshFormData.canContact ? "yes" : "no",
        can_share_contact: mshFormData.canShareContact ? "yes" : "no",
        email: mshFormData.email, // required field accoding to API but will work without
        phone: mshFormData.phone, // required field accoding to API but will work without
        ipAddress: ipAddress,
        testreport: Constants.SUBMIT_REPORT_TO_MSH_AS_TEST,
        channel: Constants.MSH_REPORTING_CHANNEL,
      };

      const formBody = Object.keys(postBody)
        .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(postBody[key]))
        .join("&");

      console.log("BODY TO MSH:");
      console.log(JSON.stringify(postBody));

      const options = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: formBody,
      };

      const res = await fetch(Constants.ENDPOINT_SEND_REPORT_MSH, options);
      const json = await res.json();

      return true;
    } catch (error) {
      console.log("Failed to submit report to Modern Slavery Helpline. Error: ", error);
      return false;
    }
  }

  export async function submitReportToAimerServer(): Promise<boolean> {
    try {
      const score = calculateTotalScore();
      const dateTimeCreated = new Date().toDateString();
      const manualAddress: any = {};
      data.addressFields.forEach(addr => {
        const key = addr.text.replace(/\s+/g, "");
        manualAddress[key] = addr.answer;
      });
      const questionAndAnswers = buildQuestionsAndAnswers();
      const showMSH = isModernSlaveryLikely() ? "YES" : "NO";

      const postBody = {
        test: Constants.SUBMIT_REPORT_TO_AIMER_AS_TEST,
        location: {
          latitude: location.latitude,
          longitude: location.longitude,
        },
        score: score,
        dateTimeCreated: dateTimeCreated,
        detectedAddress: {
          streetNumber: address.streetNumber,
          route: address.route,
          city: address.city,
          district: address.district,
          postalCode: address.postalCode,
          region: address.region,
          country: address.country,
        },
        manualAddress: manualAddress,
        questionAndAnswers: questionAndAnswers,
        showMSH: showMSH,
      };

      console.log("POST BODY TO AIMER SERVER:");
      console.log(JSON.stringify(postBody));

      const options = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "SCW-key": Constants.AIMER_REPORT_AUTH_KEY,
        },
        body: JSON.stringify(postBody),
      };

      const res = await fetch(Constants.ENDPOINT_SEND_REPORT_AIMER, options);
      const json = await res.json();

      if (json.success) {
        reportID = json.results.ID;
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.log("Failed to submit report to Aimer server. Error: ", error);
      return false;
    }
  }

  export async function markReportAsSentToMSH(): Promise<boolean> {
    try {
      const options = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "SCW-key": Constants.AIMER_REPORT_AUTH_KEY,
        },
        body: JSON.stringify({ ID: reportID }),
      };
      const res = await fetch(Constants.ENDPOINT_MARK_REPORT_AS_SENT, options);
      const json = await res.json();
      // console.log("markReportAsSentToMSH() result: ", json);

      return true;
    } catch (error) {
      console.log("Failed to submit report to Aimer server. Error: ", error);
      return false;
    }
  }

  export async function sendCounty(county: string): Promise<boolean> {
    try {
      const options = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "SCW-key": Constants.AIMER_REPORT_AUTH_KEY,
        },
        body: JSON.stringify({ county: county }),
      };
      const res = await fetch(Constants.ENDPOINT_SEND_COUNTY, options);
      const json = await res.json();
      // console.log("sendCounty() result: ", json);

      return true;
    } catch (error) {
      console.log("Failed to send county to Aimer server. Error: ", error);
      return false;
    }
  }
}
