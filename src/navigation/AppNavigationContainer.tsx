import React from "react";
import { createStackNavigator, StackNavigationOptions, TransitionPresets } from "@react-navigation/stack";
import { Dimensions, Image, useWindowDimensions, View } from "react-native";
import { DataController } from "../controllers/DataController";
import { Colors } from "../utils/Colors";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Landing from "../components/Landing";
import Onboarding from "../components/onboarding/Onboarding";
import { UtilFunctions } from "../utils/UtilFunctions";
import { NavigationContainer } from "@react-navigation/native";
import CompleteDemo from "../components/report/CompleteDemo";
import Complete from "../components/report/Complete";
import ReportSubmissionDetails from "../components/report/ReportSubmissionDetails";
import Location from "../components/report/Location";
import ReportThankYou from "../components/report/ReportThankYou";
import Menu from "../components/menu/Menu";
import QuestionsTemplatePage from "../components/report/QuestionsTemplatePage";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const mainScreenOptions = (): StackNavigationOptions => {
  return {
    gestureEnabled: false,
    headerTitle: () => (
      <View style={{ width: "100%", backgroundColor: Colors.P_20 }}>
        <Image style={{ width: 44, height: 44 }} source={require("../../assets/images/logo.png")} />
      </View>
    ),
    headerTitleAlign: "center",
    headerStyle: {
      backgroundColor: Colors.P_20,
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: Colors.P_80,
    ...TransitionPresets.SlideFromRightIOS,
  };
};

const MainStack = ({ route }: any) => {
  const data = DataController.getData();

  return (
    <Stack.Navigator
      initialRouteName={route.params.onboardingComplete ? "Landing" : "Onboarding"}
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen name="Onboarding" component={Onboarding} options={{ headerShown: false }} />
      <Stack.Screen name="Landing" component={Landing} options={mainScreenOptions} />
      {data.reportingPages.map(page => {
        return (
          <Stack.Screen
            key={page.id}
            name={page.name}
            component={page.name === "LOCATION" ? Location : QuestionsTemplatePage}
            options={mainScreenOptions}
          />
        );
      })}
      <Stack.Screen name="Complete" component={Complete} options={mainScreenOptions} />
      <Stack.Screen name="CompleteDemo" component={CompleteDemo} options={mainScreenOptions} />
      <Stack.Screen name="ReportSubmissionDetails" component={ReportSubmissionDetails} options={mainScreenOptions} />
      <Stack.Screen name="ReportThankYou" component={ReportThankYou} options={mainScreenOptions} />
    </Stack.Navigator>
  );
};

export default ({ onboardingComplete }: any) => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={{
          swipeEdgeWidth: 0,
          headerShown: false,
          gestureEnabled: false,
          drawerStyle: {
            width: UtilFunctions.isSmallViewport() ? Dimensions.get("window").width : 375,
          },
        }}
        drawerContent={props => <Menu {...props} />}
      >
        <Drawer.Screen
          name="MainStack"
          component={MainStack}
          initialParams={{ onboardingComplete: onboardingComplete }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
