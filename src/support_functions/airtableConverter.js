var Airtable = require("airtable");
var fs = require("fs");
Airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: "keywtvWj9DBY07iiw",
});

const AIRTABLE_REF = "appcixI4cRBH9qKbx";

main();
async function main() {
  let lastModifiedTime = 1;

  let reportingPagesAT = [];
  let questionsAT = [];
  let menuItemsAT = [];
  let completedPagesAT = [];
  let addressFieldsAT = [];
  let miscellaneousImagesAT = [];
  let onboardingPagesAT = [];

  await loadATBase("Reporting Pages", "Grid view", reportingPagesAT);
  await loadATBase("Questions", "Grid view", questionsAT);
  await loadATBase("Menu Items", "Grid view", menuItemsAT);
  await loadATBase("Completed Pages", "Grid view", completedPagesAT);
  await loadATBase("Address Fields", "Grid view", addressFieldsAT);
  await loadATBase("Miscellaneous Images", "Grid view", miscellaneousImagesAT);
  await loadATBase("Onboarding Pages", "Grid view", onboardingPagesAT);

  let jasonstr = SaveAsJSON(
    reportingPagesAT,
    questionsAT,
    menuItemsAT,
    completedPagesAT,
    addressFieldsAT,
    miscellaneousImagesAT,
    onboardingPagesAT
  );

  fs.writeFileSync("assets/json/ATData.json", jasonstr);
}

function SaveAsJSON(
  reportingPagesAT,
  questionsAT,
  menuItemsAT,
  completedPagesAT,
  addressFieldsAT,
  miscellaneousImagesAT,
  onboardingPagesAT
) {
  lastModifiedTime = new Date(1);
  let reportingPages = convertATArrayToJson(reportingPagesAT);
  let questions = convertATArrayToJson(questionsAT);
  let menuItems = convertATArrayToJson(menuItemsAT);
  let completedPages = convertATArrayToJson(completedPagesAT);
  let addressFields = convertATArrayToJson(addressFieldsAT);
  let miscellaneousImages = convertATArrayToJson(miscellaneousImagesAT);
  let onboardingPages = convertATArrayToJson(onboardingPagesAT);

  let allTogether = {
    updated: lastModifiedTime,
    reportingPages: reportingPages,
    questions: questions,
    menuItems: menuItems,
    completedPages: completedPages,
    addressFields: addressFields,
    miscellaneousImages: miscellaneousImages,
    onboardingPages: onboardingPages,
  };
  let jsonStr = JSON.stringify(allTogether, null, 2);

  return jsonStr;
}

function convertATArrayToJson(ATArray) {
  let result = [];

  for (let aElement of ATArray) {
    let aJSONElement = { id: aElement.id, fields: aElement.fields };
    if (aElement.fields["LastModifiedTime"]) {
      let lmt = new Date(aElement.fields["LastModifiedTime"]);
      if (lmt.getTime() > lastModifiedTime.getTime()) lastModifiedTime = lmt;
    }
    result.push(aJSONElement);
  }
  return result;
}

function loadATBase(tableName, viewName, destArray) {
  const base = Airtable.base(AIRTABLE_REF);

  var sectionPromise = new Promise(function (resolve, reject) {
    base(tableName)
      .select({
        // Selecting the first x records in Grid view:
        // maxRecords: 5000,
        view: viewName,
      })
      .eachPage(
        // This function (`page`) will get called for each page of records.
        function page(records, fetchNextPage) {
          records.forEach(function (record) {
            record.fields.base = base;
            destArray.push(record);
          });

          // To fetch the next page of records, call `fetchNextPage`.
          // If there are more records, `page` will get called again.
          // If there are no more records, `done` will get called.
          fetchNextPage();
        },
        function done(err) {
          // check for error
          if (err) {
            // reject(destArray);
            console.error(err);
          }

          // resolve promise
          resolve(destArray);
        }
      );
  });
  return sectionPromise;
}
